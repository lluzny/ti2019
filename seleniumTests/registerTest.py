from selenium import webdriver
import commonActions
import time

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000')

menu = driver.find_element_by_class_name('MuiButtonBase-root-50')
menu.click()

menuElements = driver.find_elements_by_class_name('MuiButtonBase-root-50')

menuElements[2].click()

commonActions.registerInput(driver, 'test1042', 'Test1042', 'Test1042', 'Test', 'TysiacCzterdziesciDwa', 'test1042@zut.edu')

driver.save_screenshot('register_ok.png')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(3)

print('Success')

driver.quit()