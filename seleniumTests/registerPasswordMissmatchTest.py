from selenium import webdriver
import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000/register')

commonActions.registerInput(driver, 'test4242', 'Test4242', 'Test4243', 'TestCzterdziesciDwa', 'TestowyCzterdziesciDwa', 'test42@zut.edu')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')

driver.save_screenshot('password_missmatch.png')

try:
    buttons[1].click()
    print('ERROR')
except Exception:
    msg = driver.find_element_by_class_name('MuiFormHelperText-root-301')
    if (msg.text == 'Passwords must be the same'):
        print('Success')

driver.quit()