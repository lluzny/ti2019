from selenium import webdriver
import time
import commonActions
from datetime import datetime

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000/login')

commonActions.loginInput(driver, 'test1042', 'Test1042') # good login

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(2)

commonActions.closeMsg(driver)

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(1)

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[3].click()

time.sleep(3)

driver.get('localhost:3000/login')

commonActions.loginInput(driver, 'test1042', 'wrongPassword1') # bad login

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(2)

driver.get('localhost:3000/login')

commonActions.loginInput(driver, 'test1042', 'Test1042') # good login

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(2)

commonActions.closeMsg(driver)

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(1)

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[2].click()

time.sleep(3)

headers = driver.find_elements_by_xpath('//h4')

driver.save_screenshot('profile.png')

if len(headers) == 6:
    successLoginSplit = str(headers[4].text).split(' ')
    failureLoginSplit = str(headers[5].text).split(' ')
    successLogin = successLoginSplit[len(successLoginSplit) - 1]
    failureLogin = failureLoginSplit[len(failureLoginSplit) - 1]

    dateSuccess = datetime.strptime(successLogin, '%Y-%m-%dT%H:%M:%S')
    dateFailure = datetime.strptime(failureLogin, '%Y-%m-%dT%H:%M:%S')

    val = dateFailure - dateSuccess
    if (int(val.seconds) < 120):
        print('Success')
    else:
        print('ERROR')
else:
    print('ERROR')

driver.quit()