from selenium.webdriver.common.keys import Keys
import time

def registerInput(driver, username, password, confirmPassword, name, surname, email):
    usernameInput = driver.find_element_by_name('username')
    usernameInput.send_keys(username)
    passwordInput = driver.find_element_by_name('password')
    passwordInput.send_keys(password)
    confirmPasswordInput= driver.find_element_by_name('confirmPassword')
    confirmPasswordInput.send_keys(confirmPassword)
    nameInput = driver.find_element_by_name('name')
    nameInput.send_keys(name)
    surnameInput = driver.find_element_by_name('surname')
    surnameInput.send_keys(surname)
    emailInput = driver.find_element_by_name('email')
    emailInput.send_keys(email)
    emailInput.send_keys(Keys.TAB)

def loginInput(driver, username, password):
    usernameInput = driver.find_element_by_name('username')
    usernameInput.send_keys(username)
    passwordInput = driver.find_element_by_name('password')
    passwordInput.send_keys(password)

def checkMessage(driver, expectedText):
    msgXPath = "//div/h3"
    msg = driver.find_elements_by_xpath(msgXPath)
    return msg[1].text == expectedText

def closeMsg(driver):
    close = driver.find_elements_by_class_name('MuiIconButton-label-49')
    close[len(close) - 1].click()
    time.sleep(2)