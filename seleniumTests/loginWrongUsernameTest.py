from selenium import webdriver
import time
#from selenium.webdriver.support import ui
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.by import By

import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000')

menu = driver.find_element_by_class_name('MuiButtonBase-root-50')
menu.click()

menuElements = driver.find_elements_by_class_name('MuiButtonBase-root-50')

menuElements[1].click()

commonActions.loginInput(driver, 'test444', 'Test1042')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(1)

driver.save_screenshot('wrong_username.png')

if (commonActions.checkMessage(driver,'Bad credentials')):
    print('Success')
else:
    print('ERROR')

driver.quit()

