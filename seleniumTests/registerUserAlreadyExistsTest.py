from selenium import webdriver
import time
import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000')

menu = driver.find_element_by_class_name('MuiButtonBase-root-50')
menu.click()

menuElements = driver.find_elements_by_class_name('MuiButtonBase-root-50')

menuElements[2].click()

commonActions.registerInput(driver, 'test1042', 'Test1042', 'Test1042', 'TestCzterdziesciDwa', 'TestowyCzterdziesciDwa', 'test42@zut.edu')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(1)

msgXPath = "//div/h3"
msg = driver.find_elements_by_xpath(msgXPath)

driver.save_screenshot('username_in_use.png')

if (msg[1].text == 'Username already in use.'):
    print('Success')
else:
    print('ERROR')

driver.quit()

