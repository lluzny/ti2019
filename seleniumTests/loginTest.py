from selenium import webdriver
import time
#from selenium.webdriver.support import ui
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.by import By

import commonActions

location = './chromedriver'
#options = webdriver.ChromeOptions()
driver = webdriver.Chrome(location)

driver.get('localhost:3000')

menu = driver.find_element_by_class_name('MuiButtonBase-root-50')
menu.click()

menuElements = driver.find_elements_by_class_name('MuiButtonBase-root-50')

menuElements[1].click()

commonActions.loginInput(driver, 'test1042', 'Test1042')

driver.save_screenshot('login_ok_data.png')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(3)

commonActions.closeMsg(driver)

driver.save_screenshot('login_ok.png')

hello = driver.find_elements_by_class_name('MuiTypography-root-66 ')

msg = str(hello[1].text)
if msg.startswith('Hi'):
    print('Success')
else:
    print('Error!')


driver.quit()

