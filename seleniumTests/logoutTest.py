from selenium import webdriver
import time
import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000/login')

commonActions.loginInput(driver, 'test1042', 'Test1042')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(3)

close = driver.find_elements_by_class_name('MuiIconButton-root-44')
close[len(close) - 1].click()

time.sleep(2)

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[1].click()

time.sleep(1)

driver.save_screenshot('logout_menu.png')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
buttons[3].click()

time.sleep(2)

divs = driver.find_elements_by_xpath('//div')

msg = str(divs[0].text)
msg = msg.split('\n')

driver.save_screenshot('logout_ok.png')

if (msg[1] == 'You are not authorized! Please log in first'):
    print('SUCCESS')
else:
    print('ERROR')

driver.quit()

