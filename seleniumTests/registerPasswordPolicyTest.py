from selenium import webdriver
import time
import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000/register')

commonActions.registerInput(driver, 'test898', 'test898', 'test898', 'Test', 'Testowy', 'test898@zut.edu')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')

driver.save_screenshot('password_policy.png')

try:
    buttons[1].click()
    print('ERROR')
except Exception:
    msg = driver.find_elements_by_class_name('MuiFormHelperText-root-301')
    if (msg[0].text == 'Password need to contain minimum eight characters, at least one uppercase letter, one lowercase letter and one number'):
        print('Success')

driver.quit()