from selenium import webdriver
import time
#from selenium.webdriver.support import ui
#from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.common.by import By

import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

for i in range(0,5):

    driver.get('localhost:3000/login')

    username = 'test1042'
    password = 'wrongPassword1'

    commonActions.loginInput(driver, username, password)

    buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')
    buttons[1].click()

    time.sleep(3)


driver.save_screenshot('blocked_user.png')

if (commonActions.checkMessage(driver,'User with id: ' + username +
    ' is banned from login to shopping list app for 24 hours because of 5 unsuccessful login attempts in a row.')):
    print('Success')
else:
    print('ERROR')

driver.quit()

