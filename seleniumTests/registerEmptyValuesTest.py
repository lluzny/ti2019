from selenium import webdriver
import time
import commonActions

location = './chromedriver'
driver = webdriver.Chrome(location)

driver.get('localhost:3000/register')

commonActions.registerInput(driver, '', '', '', '', '', '')

buttons = driver.find_elements_by_class_name('MuiButtonBase-root-50')

driver.save_screenshot('empty_values.png')

try:
    buttons[1].click()
    print('ERROR')
except Exception:
    msg = driver.find_elements_by_class_name('MuiFormHelperText-root-301')
    if (msg.__len__() == 6):
        print('Success')

driver.quit()