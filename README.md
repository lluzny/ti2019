# PBAI2019

Projekt grupowy z przedmiotu Projektowanie bezpiecznych aplikacji internetowych

Wymagania:
- Java 8 (jre 1.8)
- node 10 lub wyższa
- baza danych MySQL(pracująca na porcie 3306)
- maven 3.3.9

Instrukcje uruchomienia:
1. W bazie danych utwórz scheme o nazwie pbai
2. W pliku application.xml w ścieżce spring.datasource zmień username oraz password.
3. Zaciągnij oraz uruchom zależności frontendowe:
```sh
$ cd frontend
$ npm install
$ npm start
```
4. Zaimportuj projekt backendowy w swoim IDE (IntelliJ, Eclipse)
5. Wystartuj backend uruchamiając metodę main znajdującą się w klasie PBAI2019Application.java