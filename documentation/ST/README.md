# Opracowanie zadania zabezpieczeń systemu do bezpiecznego współdzielenia listy zakupów

## Spis treści
* [Wstęp](#wstęp)
* [Wprowadzenie](#wprowadzenie)
* [Opis przedmiotu oceny](#opis-przedmiotu-oceny)
* [Środowisko zabezpieczeń TOE](#środowisko-zabezpieczeń-toe)
* [Cele zabezpieczeń](#cele-zabezpieczeń)
* [Wymagania bezpieczeństwa](#wymagania-bezpieczeństwa)
* [Specyfikacja funkcjonalna TOE](#specyfikacja-funkcjonalna-toe)

## Wstęp
### Cel
Celem dokumentu zadania zabezpieczeń jest przedstawienie wymagań bezpieczeństwa nakładanych na system do bezpiecznego współdzielenia listy zakupów. Podczas opracowywania pod uwagę wzięto obowiązujące normy, zalecenia i szkice dostępne w momencie powstawania dokumentu.  

## Wprowadzenie
Przedstawiony poniżej dokument zadania zabezpieczeń określa wymagania bezpieczeństwa dla systemu bezpiecznego dzielenia się listami zakupów. System ten realizuje tworzenie i udostępnianie w bezpieczny sposób list zakupów.  

### Identyfikacja

**Tytuł:** Zadanie zabezpieczeń - system do bezpiecznego dzielenia się listami zakupów  
**Autorzy:** Rafał Drzewowski, Bartosz Kopciuch, Łukasz Łużny, Sławomir Marcinkiewicz, Patryk Strusiewicz-Surmacki, Jacek Wolny  
**Status głosowania:** CC Version: 1.0 (Kwiecień 2019)  
**Ogólny status:** Draft  
**Numer wersji:** 1.0  
**Słowa kluczowe:** kryptografia asymetryczna, współdzielenie danych, funkcja skrótu  

## Opis przedmiotu oceny
Ta część profilu zabezpieczeń zawiera opis przedmiotu oceny (TOE), rodzaj produktu, który prezentuje, jak również opis ogólnej funkcjonalności TOE. Przedstawiona funkcjonalność, podlegająca ocenie, dotyczy udostępniania danych po zabezpieczonym kanale oraz kontroli autoryzowanego dostępu do nich.  

### Opis TOE
Przedmiotem oceny, rozważanym w niniejszym ST są następujące komponenty: “Rejestracja”, “Logowanie”, “Moje listy”, “Stwórz listę”, “Udostępnij listę”, “Logi”. Wchodzą one w skład systemu do bezpiecznego dzielenia się listami zakupów.  

![toe_diagram](https://gitlab.com/lluzny/ti2019/raw/develop/documentation/ST/app_diagram.svg)

#### Opis komponentów
**Rejestracja:**
* rejestracja użytkownika (pola “Login”, “Hasło”, “Powtórz hasło”, “Adres email”),
* weryfikacja poprawności danych (prawidłowy format adresu email, hasło zgodne z wymaganiami, powtórzone hasło identyczne jak hasło),
* utworzenie konta użytkownika po spełnieniu powyższych wymagań,
* wykorzystuje protokół HTTPS.

**Logowanie:**
* logowanie do konta użytkownika (pola “Login” i “Hasło”),
* weryfikacja powyższych danych,
* wykorzystuje protokół HTTPS.

**Moje listy:**
* wyświetlanie utworzonych wcześniej list i list udostępnionych użytkownikowi,
* zarządzanie listami (wybór opcji utworzenia, usunięcia, edycji i udostępnienia).

**Stwórz listę:**
* tworzenie listy,
* zapisywanie listy do bazy danych.

**Udostępnij listę:**
* wskazanie użytkownika bądź użytkowników, którym należy udostępnić listę,
* ustalenie uprawnień dla tychże użytkowników,
* ustanowienie bezpiecznego połączenia,
* udostępnienie listy,
* odbiór udostępnionej listy.

**Logi:**
* zbieranie i przechowywanie logów generowanych podczas pracy systemu,
* udostępnianie użytkownikowi logów do odczytu.

## Środowisko zabezpieczeń TOE

### Aktywa
W tej sekcji opisano wszystko aktywa chronione przez TOE.  

**A. Dokument**  
Dokument (lista zakupów) do bezpiecznego udostępniania, który może się składać z:
* pojedynczego dokumentu elektronicznego,
* wielu dokumentów elektronicznych.
Dane zawarte w dokumencie muszą być chronione przed utratą integralności i/lub poufności.   

**A. Dane uwierzytelniające podmiotu używającego system**  
Są to dane, które pozwalają podmiotowi używającemu system na uwierzytelnienie się (za pomocą loginu i hasła). Hasło musi spełniać ustalone warunki (długość minimum 8 znaków; zawiera przynajmniej po jednej wielkiej literze, małej literze i cyfrze). Pomyślne zakończenie uwierzytelnienia przeprowadza system w stan gotowości do wykonania poleceń uwierzytelnionego podmiotu.  
Dane te muszą być chronione przed utratą integralności i poufności.   

**A. Szyfrogram**  
Szyfrogram jest zagregowanym zbiorem danych, zawierającym:
* komplet danych do szyfrowania;
* dodatkowe informacje ułatwiające odszyfrowanie szyfrogramu, w tym atrybuty szyfrogramu.
Aktywa te muszą być chronione przez TOE w trakcie ich tworzenia i przed ich przekazaniem podmiotowi szyfrującemu.  

**A. Polityka szyfrowania**  
Polityki szyfrowania definiują reguły, które powinny być stosowane podczas szyfrowania danych i ich deszyfrowania. Lista polityk szyfrowania, zarządzana przez administratora TOE, musi być chroniona przed utratą integralności. Dodatkowo, kontrolowana musi być integralność każdej z polityk szyfrowania.  
Dane te muszą być chronione przed utratą integralności.  

**A. Zgodność formatu dokumentu z jego przeglądarką**  
Mechanizmy zaimplementowane w TOE zarządzają parametrami, które pozwalają TOE na uruchomienie właściwej przeglądarki, obsługującej format wskazanego dokumentu i poprawne zaprezentowanie jego treści podmiotowi używającemu system.  
Parametry te muszą być chronione przed utratą integralności.  

### Podmioty Systemu
Ta sekcja opisuje podmioty TOE.  

**S. Użytkownicy**  
Podmiot szyfrujący/deszyfrujący i udostępniający współdziałają z TOE podczas operacji szyfrowania/deszyfrowania i udostępniania listy zakupów, wykonywanych zgodnie z polityką udostępniania lub szyfrowania dla jednego lub kilku dokumentów.  

**S. Administrator**  
Administrator posiada niezbędne środki i jest przeszkolony w zakresie wykonywania wszelkich operacji na TOE, za które jest odpowiedzialny; wykonuje stałą obsługę systemu teleinformatycznego, w tym także tworzy kopie zapasowe, zdalnie umieszcza kopie archiwów oraz bieżące kopie zapasowe poza podstawowym obszarem lokalizacji TOE.  

### Założenia
W sekcji tej opisano założenia dotyczące środowiska zabezpieczeń TOE.  

**AE. Autoryzowani użytkownicy**  
Zakłada się, że zaufani użytkownicy TOE są uprawnieni do wykonywania przypisanych im czynności.  

**AE. Konfiguracja TOE**  
Zakłada się, że TOE jest poprawnie zainstalowany i skonfigurowany (zainstalowany jest najnowszy system operacyjny, najnowsza wersja wspieranej przeglądarki internetowej, zapewniona ochrona antywirusowa, poprawnie skonfigurowana polityka bezpieczeństwa i zapora systemowa itp.).  

**AE. Uwierzytelnienie**  
Zakłada się, że środowisko teleinformatyczne związane z TOE pozwala użytkownikom na uwierzytelnienie się za pomocą indywidualnych danych uwierzytelniających (login i hasło).  

**AE. Przeglądanie danych rejestrowanych w rejestrze zdarzeń**  
Zakłada się, że środowisko umożliwia upoważnionym podmiotom przeglądanie i analizowanie zapisów w rejestrze zdarzeń.  

**AE. Aktualizowanie środowiska**  
Zakłada się, że środowisko jest regularnie aktualizowane do najnowszych wersji. Ma to na celu pozbycie się zagrożenia ze strony zidentyfikowanych błędów w środowisku.  

**AE. Ochrona danych**  
Zakłada się, że dane są stale zabezpieczone i regularnie archiwizowane.  

### Zagrożenia
Ta sekcja opisuje zagrożenia mające wpływ na TOE.  

**T. Uszkodzenie TOE**  
Jeszcze przed rozpoczęciem procesu szyfrowania lub deszyfrowania przypadkowemu lub celowemu uszkodzeniu może ulec jedna lub kilka funkcji i/lub jeden lub kilka parametrów TOE.  
Przypadkowe uszkodzenie funkcji i/lub parametrów TOE może nastąpić na przykład wtedy, gdy awarii ulegnie moduł szyfrujący/deszyfrujący bądź gdy uszkodzeniu ulegnie dokument (lista zakupów). Celowe uszkodzenie może nastąpić w wyniku prób modyfikacji komponentów TOE przez atakującego, np. za pomocą zainstalowanych fałszywych programów lub aplikacji bez wiedzy użytkowników TOE. Uszkodzenie to może prowadzić do:
* utworzenia niepoprawnego szyfrogramu,
* utworzenia szyfrogramu bez wiedzy użytkownika,
* utworzenie szyfrogramu za pomocą innej niż wybrana polityki szyfrowania,
* uszkodzenia zaszyfrowanych lub deszyfrowanych danych,
* modyfikacji zawartości listy bez zgody, a nawet wiedzy użytkownika.

**T. Nieautoryzowany dostęp do bazy danych**  
Atakujący może uzyskać nieautoryzowany (z pominięciem uwierzytelnienia) dostęp do bazy danych wykorzystując atak typu SQL Injection bądź innego rodzaju obejście dla systemu bazy danych.  

**T. Nieupoważniony dostęp do systemu**  
Atakujący może uzyskać nieautoryzowany dostęp do systemu i jego pełnej funkcjonalności.  

**T. Nieupoważniony dostęp do prywatnych list zakupów**  
Atakujący może uzyskać nieautoryzowany dostęp do prywatnych list zakupów użytkownika.  

**T. Zbyt słaby algorytm szyfrujący**  
Szyfrogram może zostać utworzony za pomocą zbyt słabego algorytmu szyfrującego.  

**T. Modyfikacja uprawnień listy zakupów**  
Atakujący może zmodyfikować poziom uprawnień listy zakupów dla poszczególnych użytkowników.  

**T. Modyfikacja dostępu do listy zakupów**  
Atakujący może zmienić listę użytkowników, którym udostępniono listę zakupów.  

**T. Przypadkowe usunięcie listy zakupów**  
 Użytkownik (właściciel bądź użytkownik, któremu udostępniono listę z uprawnieniami do modyfikacji i usuwania) może przypadkowo usunąć listę, przez co wszyscy użytkownicy mający dostęp do listy utracą go.  

**T. Przejęcie konta administratora**  
Atakujący może uzyskać dostęp do konta administratora i tym samym przejąć władzę nad systemem.  

### Polityki bezpieczeństwa instytucji
W tym rozdziale określono zasady natury organizacyjnej, mające zastosowanie do TOE.  

**P. Algorytm funkcji skrótu**  
Użyty przez TOE algorytm funkcji skrótu TOE powinien być wybrany tak, aby prawdopodobieństwo utworzenia takiego samego skrótu dla dwóch różnych dokumentów było znikome (odporność na kolizje).  

**P. Przerwanie procesu**  
Podmiot szyfrujący/deszyfrujący musi mieć możliwość przerwania procesu szyfrowania/deszyfrowania przed aktywacją klucza szyfrującego/deszyfrującego.  

**P. Integralność danych użytkownika**  
TOE musi chronić integralność wszystkich danych (lista zaszyfrowanych list zakupów, lista osób uprawnionych do wglądu, uprawnienia tychże osób itp.), przychodzących od użytkownika.  

**P. Eksport szyfrogramu**  
Po zakończeniu procesu szyfrowania powstały w jego wyniku szyfrogram dokumentu musi zostać przekazany przez TOE podmiotowi szyfrującemu/deszyfrującemu. Przekazane dane muszą zawierać przynajmniej:
* szyfrogram,
* zaszyfrowane dane,
* atrybuty szyfrogramu.

**P. Zarządzanie**  
TOE musi pozwolić podmiotowi szyfrującemu/deszyfrującemu oraz administratorowi na zarządzanie politykami szyfrowania (ich dodawanie i usuwanie) oraz tabelą wiążącą format dokumentu z jego przeglądarką.  

**P. Algorytmy kryptograficzne**  
Do zarządzania kluczami (tj. generowania, udostępniania, niszczenia, korzystania i przechowywania kluczy) oraz udostępniania algorytmów szyfrowych (funkcji szyfrowania, deszyfrowania, podpisywania, obliczania skrótów, wymiany kluczy oraz generowania liczb losowych) stosowane mogą być tylko te algorytmy kryptograficzne (metody i ich implementacje), które spełniają wymagania określone w Rozporządzeniu Rady Ministrów z dnia 7 sierpnia 2002 r. (Dz. U. Nr 128, poz.1094 z dnia 12 sierpnia 2002 r.) oraz w Ustawie z dnia 22 stycznia 1999 r. o ochronie informacji niejawnych (Dz.U. 1999 nr 11 poz. 95, wersja ujednolicona) i zatwierdzona przez odpowiednie instytucje certyfikujące przy wysokim poziomie siły funkcji zabezpieczającej lub przynajmniej zgodne z FIPS 140 poziom 2 lub wyższy.  

## Cele zabezpieczeń

### Cele zabezpieczeń dla TOE

**O. Integralność usług**  
Zanim TOE umożliwi użytkownikom dostęp do realizowanych przez siebie funkcji, powinien najpierw sprawdzić
ich integralność oraz integralność parametrów niezbędnych do ich prawidłowego wykonania.

**O. Uwierzytelnienie użytkownika**  
TOE powinien zapewnić, aby użytkownik miał możliwość wprowadzenia danych uwierzytelniających (uwierzytelnienia się) przed uzyskaniem dostępu do prywatnych oraz udostępnionych list zakupów.

**O. Ochrona kanału komunikacyjnego**  
TOE zapewnia, że dane przesyłane między serwerem WWW a przeglądarką są chronione przed nieautoryzowanym dostępem. TOE musi zagwarantować, że nie ulegną modyfikacji w trakcie ich transferu między węzłami kanału komunikacyjnego.

**O. Integralność danych do szyfrowania**  
TOE musi zapewnić integralność różnych reprezentacji danych przeznaczonych do zaszyfrowania od momentu ich sformatowania do momentu utworzenia szyfrogramu.

**O. Ochrona procesów**  
TOE musi zapewnić ochronę przed ingerencją dowolnych niezaufanych procesów, urządzeń peryferyjnych i kanałów komunikacyjnych oraz intruzów w pracę tych procesów, które wykorzystywane są podczas szyfrowania/deszyfrowania, zgodnie ze wskazaniem zawartym w żądaniu utworzenia szyfrogramu.

**O. Zatwierdzone algorytmy**  
TOE powinien zapewnić, aby były stosowane tylko te algorytmy szyfrowe, które należą do zbioru zatwierdzonych algorytmów i parametrów stosowanych podczas tworzenia szyfrogramu; w szczególności, aby format był zgodny z formatami wskazanymi w Rozporządzeniu Rady Ministrów z dnia 7 sierpnia 2002 r. (Dz. U. Nr 128, poz.1094 z dnia 12 sierpnia 2002 r.).

**O. Poufność danych uwierzytelniających**  
TOE musi zapewnić poufność danych uwierzytelniających należących do podmiotu szyfrującego/deszyfrującego.

**O. Hashowanie hasła wraz z domieszką**  
TOE powinien szyfrować wrażliwe dane logowania użytkowników hashem bcrypt wraz z zastosowaniem domieszki (salt).

**O. Udostępnienie list zakupów innemu użytkownikowi**  
TOE powinien zapewnić podmiotowi będącemu właścicielem danej listy na udostępnienie wybranego zasobu odbiorcy wskazanego przez nadawcę.

**O. Zbiór list**  
Po wyrażeniu przez podmiot szyfrujący zgody na szyfrowanie, TOE musi gwarantować, że przetwarzana lista rzeczywiście odpowiada dokładnie wybranej liście przeznaczonej do szyfrowania.

**O. Blokowanie ataków na TOE**  
TOE musi zapewnić mechanizm blokowania ataków (wiele nieudanych prób logowania, DDoS) poprzez oflagowanie adresu IP bądź identyfikatora atakującego i zablokowanie kanału komunikacyjnego z podmiotem atakującym.

**O. Zgodność uprawnień do list**  
TOE musi zapewnić zgodność, która potwierdza uprawnienia użytkownika do pobrania/wyświetlenia wybranej listy.

**O. Zgoda użytkownika**  
TOE musi zadbać o potwierdzenie przez użytkownika systemu operacji takich jak usunięcie, modyfikacja czy udostępnienie listy.  

### Cele zabezpieczeń dla środowiska

**OE. Wiarygodni użytkownicy**  
Upoważnieni użytkownicy rzetelnie wykonują swoje zadania.

**OE. Wiarygodni administratorzy**  
Upoważnieni administratorzy rzetelnie wykonują swoje zadania.

**OE. Konfiguracja TOE**  
TOE musi być poprawnie zainstalowany i skonfigurowany tak, aby zaraz po uruchomieniu przechodził w bezpieczny stan.

**OE. Bezpieczna komunikacja**  
Komunikacja pomiędzy serwerem WWW, a przeglądarką WWW odbywa się z zastosowaniem protokołu HTTPS z TLS.

**OE. Moduły kryptograficzne**  
TOE musi korzystać tylko z tych usług kryptograficznych, udostępnianych przez środowisko teleinformatyczne, które spełniają wymagania określone w Rozporządzeniu Rady Ministrów z dnia 7 sierpnia 2002 r. (Dz. U. Nr 128, poz.1094 z dnia 12 sierpnia 2002 r.) oraz Ustawie z dnia 22 stycznia 1999 r. o ochronie informacji niejawnych (Dz.U. 1999 nr 11 poz. 95, wersja ujednolicona) i zatwierdzone przez odpowiednie instytucje certyfikujące przy wysokim poziomie siły funkcji zabezpieczającej lub przynajmniej zgodne z FIPS 140 poziom 2 lub wyższy.

**OE. Bezpieczeństwo fizyczne**  
Środowisko musi zapewniać akceptowalny poziom bezpieczeństwa fizycznego tak, aby nie było możliwe manipulowanie TOE.

**OE. Obecność użytkownika**  
Podmiot szyfrujący/deszyfrujący powinien pozostać obecny między momentem wyrażenia przez niego zamiaru szyfrowania, a momentem kiedy wprowadza dane szyfrujące.

**OE. Tworzenie danych na potrzeby audytu**  
Środowisko związane z TOE zapewni możliwość zapisywania zdarzeń związanych z bezpieczeństwem TOE w rejestrze zdarzeń w sposób jednoznacznie wiążący zdarzenie z użytkownikiem, który był przyczyną wystąpienia tego zdarzenia lub zdarzenie nastąpiło podczas korzystania przez niego z TOE.

**OE. Ochrona danych rejestrowanych na potrzeby audytu**  
Środowisko związane z TOE zapewni możliwość ochrony informacji gromadzonej na potrzeby audytu.

**OE. Przeglądanie danych rejestrowanych na potrzeby audytu**  
Środowisko związane z TOE zapewni możliwość selektywnego przeglądania informacji zgromadzonej w rejestrze zdarzeń.

**OE. Aktualizacje zabezpieczeń**  
Środowisko jest automatycznie aktualizowane w celu wyeliminowania defektów w zabezpieczeniach wykrytych w oprogramowaniu wchodzących w skład środowiska.

## Wymagania bezpieczeństwa

### FAU: audyt bezpieczeństwa
**FAU_ARP.1 - Powiadomienia systemu w przypadku wykrycia potencjalnych zagrożeń bezpieczeństwa.**  
FAU_ARP.1.1 - System musi powiadomić administratora w przypadku wystąpienia zagrożenia, wysyłając automatyczną wiadomość e-mail.

**FAU_GEN.1 - Generowanie danych dot. audytu bezpieczeństwa.**  
FAU_GEN.1.1 - System powinien zapisywać do logów systemowych potencjalnie niebezpieczne zdarzenia:  
a) Włączenie/wyłączenie zapisu danych do audytowania  
b) Wydarzenia takie jak: logowanie/wylogowanie, nieudane próby logowania, nieoczekiwane wywołania funkcji systemowych, nieobsłużone wyjątki.  
FAU_GEN.1.2 - System powinien zapisywać w każdym rekordzie następujące informacje:  
a)  Data i czas wystąpienia wydarzenia,  
Typ wydarzenia (nieudane logowanie, nieobsłużony wyjątek itp.),
Podmiot, który wywołał zdarzenie,
Skutek zaistniałego zagrożenia.  
b) Informacje inne od powyższych nie będą zawarte  

**FAU_GEN.2 - Powiązanie zdarzeń z użytkownikiem.**  
FAU_GEN.2.1 - W przypadku zdarzeń audytowych wynikających z działań zidentyfikowanych użytkowników TSF powinien być w stanie powiązać każde zdarzenie podlegające audytowi z użytkownikiem, który spowodował to zdarzenie.

**FAU_SAR.1 - Przegląd danych audytowych.**  
FAU_SAR.1.1 - System musi zapewnić możliwość odczytu zarejestrowanych danych audytu administratorowi.  
FAU_SAR.1.2 - System musi zapewnić możliwość odczytu danych w formie możliwej do interpretacji przez użytkownika.  

**FAU_SAR.2 - Ograniczony przegląd danych audytowych.**  
FAU_SAR.2.1 - System musi zabronić dostępu do przeglądu danych audytowych wszystkim użytkownikom z wyjątkiem tych określonych w FAU_SAR.1 (administratorom).  

### FCS: Wsparcie kryptografii  
**FCS_COP - Operacje kryptograficzne.**  
FCS_COP.1 - Operacje kryptograficzne.  
FCS_COP.1.1 - System będzie tworzył hasło zgodnie z algorytmem Bcrypt i ustalonymi minimalnymi wymaganiami dla hasła (długość minimum 8 znaków; zawiera przynajmniej po jednej wielkiej literze, małej literze i cyfrze).

### FDP: Ochrona danych użytkownika
**FDP_ACC.1 - Polityka kontroli dostępu.**  
FDP_ACC.1.1 - System na podstawie kontroli dostępu SFP (ang. Security. Function Policies, zbiór zasad bezpieczeństwa które muszą być przestrzegane w ramach TOE) musi egzekwować kontrolę dostępu do poszczególnych funkcji oraz zasobów TOE zdefiniowanych w SFP.

**FDP_ACF - Funkcje kontroli dostępu.**  
	FDP_ACF.1 - atrybuty kontroli dostępu.  
		FDP_ACF.1.1 - System musi wymuszać kontrolę dostępu zdefiniowaną w SFP bazującą na rolach przypisanych do poszczególnych podmiotów w ramach TOE.  
		FDP_ACF.1.2 - System musi egzekwować poniższe zasady w celu weryfikacji czy dany podmiot powinien uzyskać dostęp do wybranej funkcjonalności:  
		- podmiot musi być autoryzowanym podmiotem występującym w ramach TOE,
		- system musi zweryfikować rolę danego podmiotu,
		- na podstawie atrybutów dostępu przypisanych do poszczególnych ról, System powinien udzieli lub odmówić dostępu do danej funkcji TOE dla danego podmiotu.

**FDP_DAU - Uwierzytelnianie danych.**  
    FDP_DAU.1 - Podstawowe uwierzytelnianie danych  
        FDP_DAU.1.1 - System zapewni możliwość generowania dowodu, który zagwarantuje walidację obiektu (listy zakupów). Token JWT zostanie wygenerowany użytkownikowi po zalogowaniu.
        FDP_DAU.1.2 - System zapewni możliwość zweryfikowania dowodów informacji.

### FIA: Uwierzytelnianie i autoryzacja
**FIA_AFL - Błędy uwierzytelniania.**  
	FIA_AFL.1- Obsługa błędów uwierzytelniania.  
		FIA_AFL.1.1 - System musi wykrywać błędne próby logowania użytkowników (w ilości 5 prób).  
		FIA_AFL.1.2 - W przypadku wykrycia zdefiniowanej ilości niepoprawnych prób logowania danego użytkownika, system musi wykonać następujące czynności:  
		- zapisać dokładne informacje na temat adresu logowania, ilości niepoprawnych prób logowania, oraz podmiotu którego dotyczyły zdarzenie w logach systemu,  
		- zablokować możliwość logowania dla danego użytkownika na określony, zdefiniowany przez administratora czas,  
		- poinformować podmiot o nieudanych próbach logowania (tworząc odpowiednie zdarzenie - log).

**FIA_UAU - Uwierzytelnianie użytkownika.**  
    FIA_UAU.1 - Czas uwierzytelniania.  
		FIA_UAU.1.1 - System wymaga, aby każdy użytkownik został pomyślnie uwierzytelniony, zanim zdecyduje się na inne operacje związane z systemem w imieniu tego użytkownika.  
		FIA_UAU.1.2 - System wymaga, aby każdy użytkownik został zidentyfikowany przed umożliwieniem w imieniu tego użytkownika jakichkolwiek innych działań z udziałem systemu.  
    FIA_UAU.2 - Uwierzytelnianie użytkownika przed każdą akcją.  
        FIA_UAU.2.1 - System powinien uwierzytelnić użytkownika zanim będzie mógł wykonać zastrzeżone akcje w systemie, np. działania na listach zakupów.

**FIA_UID - Autoryzacja użytkownika.**  
    FIA_UID.1 - Czas autoryzacji.  
        FIA_UID.1.1 - System wymaga, aby każdy użytkownik został pomyślnie autoryzowany, zanim zdecyduje się na inne operacje związane z systemem w imieniu tego użytkownika.  
		FIA_UID.1.2 - System wymaga, aby każdy użytkownik został autoryzowany przed umożliwieniem w imieniu tego użytkownika jakichkolwiek innych działań z udziałem systemu.  
    FIA_UID.2 - Autoryzacja użytkownika przed każdą akcją.  
        FIA_UID.2.1 - System powinien autoryzować użytkownika zanim będzie mógł wykonać zastrzeżone akcje w systemie, np. działania na listach zakupów.

### FMT: Zarządzanie bezpieczeństwem  
**FMT_SMR.1 - Role zarządzania bezpieczeństwem.**  
	FMT_SMR.1 - Role bezpieczeństwa.  
		FMT_SMR.1.1 - system musi przechowywać następujące role użytkowników:  
		- użytkownik,  
		- administrator.  
		FMT_SMR.1.2 - system musi być w stanie powiązać użytkowników z odpowiednimi rolami (role wymieniono w FMT_SMR.1.1)

**FMT_MSA - Zarządzanie atrybutami bezpieczeństwa.**  
	FMT_MSA.3 - Inicjalizacja atrybutów statycznych zapewnia, że wartości domyślne atrybutów bezpieczeństwa są restrykcyjne z natury.  

### FPT: Ochrona danych TSF
**FPT_STM.1 - Znaczniki czasu.**  
FPT_STM.1.1 - System musi niezawodnie generować znaczniki czasu.  
FPT_STM.1.2 - System powinien rejestrować wszystkie akcje użytkowników w postaci logów systemowych i przypisywać im znaczniki czasu.

### FTA – Dostęp do TOE   
**FTA_TAH.1 – Historia dostępu do TOE**  
FTA_TAH.1.1 – Przy poprawnym uwierzytelnieniu się TSF powinien wyświetlić datę i czas ostatniego poprawnego uwierzytelnienia się przez użytkownika.  
FTA_TAH.1.2 – Przy poprawnym uwierzytelnieniu się TSF powinien wyświetlić datę i czas ostatnich niepoprawnych prób uwierzytelnień do czasu ostatniego poprawnego uwierzytelnienia.  
FTA_TAH.1.3 – TSF powinien wyświetlać tak w.w informacje, żeby użytkownik miał możliwość ich dokładnego przejrzenia.

## Specyfikacja funkcjonalna TOE

### Funkcje bezpieczeństwa TOE
Każdemu wymaganiu bezpieczeństwa odpowiada konkretna funkcja bezpieczeństwa. 
Każda funkcja opisana jest w sposób który opisuje, jak spełnia ona odpowiadające jej wymaganie bezpieczeństwa.

|  Funkcja bezpieczeństwa TOE	|   SFR ID |
|---	|---	|
| Alarm bezpieczeństwa	| FAU_ARP.1	|
| Audyt bezpieczeństwa  | FAU_GEN.1, FAU_GEN.2, FAU_SAR.1, FAU_SAR.2		| 
| Wsparcie kryptografii | FCS.COP.1		| 
| Ochrona danych użytkownika | 	FDP_ACC.1, FDP_AFC.1, FDP_DAU.1	 |
| Uwierzytelnianie i autoryzacja |	FIA_AFL.1, FIA_UAU.1, FIA_UAU.2, FIA_UID.1, FIA_UID.2 |
| Zarządzanie bezpieczeństwem |	FMT_SMR.1, FMT_MSA.3 | 
| Ochrona danych TSF | FPT_STM.1 |
| Dostęp do TOE | FPT_TAH.1 |

## Uzasadnienie celów zabezpieczeń
W niniejszym rozdziale zawarto uzasadnienie, dlaczego zidentyfikowane cele zabezpieczeń (rozdz. 5) są odpowiednie do przeciwdziałania zidentyfikowanym zagrożeniom i spełniają określone polityki bezpieczeństwa (rozdz. 4). Uzasadnienie to przedstawiono w formie tabel, w których umieszczono także uzasadnienie dla celów zabezpieczeń związanych ze środowiskiem TOE.

### Odwzorowanie celów zabezpieczeń TOE na politykę i zagrożenie

Cele zabezpieczeń TOE | Polityka/Zagrożenia
--- | ---
O. Integralność usług | T. Uszkodzenie TOE, P. Zarządzanie
O. Uwierzytelnienie użytkownika | T. Nieautoryzowany dostęp do bazy danych, T. Nieupoważniony dostęp do systemu, T. Nieupoważniony dostęp do prywatnych list zakupów, T. Przejęcie konta administratora
O. Ochrona kanału komunikacyjnego | P. Zarządzanie, T. Przejęcie konta administratora
O. Integralność danych do szyfrowania | T. Zbyt słaby algorytm szyfrujący
O. Ochrona procesów | T. Nieautoryzowany dostęp do bazy danych, T. Nieupoważniony dostęp do systemu, P. Przerwanie procesu
O. Zatwierdzone algorytmy | P. Algorytmy kryptograficzne, T. Zbyt słaby algorytm szyfrujący, P. Algorytm funkcji skrótu
O. Poufność danych uwierzytelniających | P. Eksport szyfrogramu
O. Hashowanie hasła wraz z domieszką | T. Nieupoważniony dostęp do systemu, T. Zbyt słaby algorytm szyfrujący
O. Udostępnienie list zakupów innemu użytkownikowi | P. Zarządzanie
O. Zbiór list | P. Integralność danych użytkownika, P. Zarządzanie
O. Blokowanie ataków na TOE | T. Uszkodzenie TOE
O. Zgodność uprawnień do list | P. Integralność danych użytkownika, T. Nieupoważniony dostęp do prywatnych list zakupów
O. Zgoda użytkownika | T. Modyfikacja uprawnień listy zakupów, T. Modyfikacja dostępu do listy zakupów, T. Przypadkowe usunięcie listy zakupów

### Odwzorowanie zagrożeń TOE na cele zabezpieczeń

Zagrożenie | Cele zabezpieczeń TOE
--- | ---
T. Uszkodzenie TOE | O. Blokowanie ataków na TOE, O. Integralność usług
T. Nieautoryzowany dostęp do bazy danych | O. Uwierzytelnienie użytkownika, O. Ochrona procesów, O. Hashowanie hasła wraz z domieszką
T. Nieupoważniony dostęp do systemu | O. Uwierzytelnienie użytkownika, O. Ochrona procesów
T. Nieupoważniony dostęp do prywatnych list zakupów | O. Uwierzytelnienie użytkownika, O. Zgodność uprawnień do list
T. Zbyt słaby algorytm szyfrujący | O. Integralność danych do szyfrowania, O. Zatwierdzone algorytmy, O. Hashowanie hasła wraz z domieszką
T. Modyfikacja uprawnień listy zakupów | O. Zgoda użytkownika
T. Modyfikacja dostępu do listy zakupów | O. Zgoda użytkownika
T. Przypadkowe usunięcie listy zakupów | O. Zgoda użytkownika
T. Przejęcie konta administratora | O. Uwierzytelnienie użytkownika, O. Ochrona kanału komunikacyjnego

### Odwzorowanie polityki zabezpieczeń TOE na cele zabezpieczeń

Polityka | Cele zabezpieczeń TOE
--- | ---
P. Algorytm funkcji skrótu | O. Zatwierdzone algorytmy
P. Przerwanie procesu | O. Ochrona procesów
P. Integralność danych użytkownika | O. Zbiór list, O. Zgodność uprawnień do list
P. Eksport szyfrogramu | O. Poufność danych uwierzytelniających
P. Zarządzanie | O. Ochrona kanału komunikacyjnego, O. Udostępnienie list zakupów innemu użytkownikowi, O. Zbiór list, O. Integralność usług
P. Algorytmy kryptograficzne | O. Zatwierdzone algorytmy

### Odwzorowanie celów zabezpieczeń TOE na wymagania funkcjonalne

Cele zabezpieczeń TOE | Wymagania funkcjonalne dla TOE
--- | ---
O. Integralność usług | FDP_ACC.1, FDP_DAU.1
O. Uwierzytelnienie użytkownika | FIA_AFL.1, FIA_UAU.1, FIA_UAU.2, FIA_UID.1, FIA_UID.2, FMT_SMR.1
O. Integralność danych do szyfrowania | FPT_STM.1, FCS_COP.1
O. Ochrona procesów |  FMT_SMR.1
O. Zatwierdzone algorytmy | FCS_COP.1,
O. Poufność danych uwierzytelniających | FDP_ACC.1, FIA_UID.1, FIA_UID.2
O. Hashowanie hasła wraz z domieszką | FCS_COP.1
O. Udostępnienie list zakupów innemu użytkownikowi | FIA_UAU.1, FIA_UAU.2
O. Zbiór list | FIA_UAU.1, FIA_UAU.2
O. Ustawienie czasu wygaśnięcia listy | FPT_STM.1
O. Blokowanie ataków na TOE | FIA_AFL.1
O. Zgodność uprawnień do list | FMT_SMR.1, FMT_MSA.3
O. Zgoda użytkownika | FIA_UID.1, FIA_UID.2

### Uzasadnienie zależności

Wymagania funkcjonalne dla TOE | Zależności | Niespełnione zależności
--- | --- | ---
FAU_ARP.1/Powiadomienia systemu w przypadku wykrycia potencjalnych zagrożeń bezpieczeństwa | FAU_SAA.1/Analiza potencjalnych naruszeń | 
FAU_SAA.1/Analiza potencjalnych naruszeń | FAU_GEN.1/Generowanie danych dot. audytu bezpieczeństwa | 
FAU_GEN.1/Generowanie danych dot. audytu bezpieczeństwa | FPT_STM.1/Znaczniki czasu | 
FAU_GEN.2/Powiązanie zdarzeń z użytkownikiem | FAU_GEN.1/Generowanie danych dot. audytu bezpieczeństwa <br> FIA_UID.1/Czas autoryzacji |
FAU_SAR.1/Przegląd danych audytowych | FAU_GEN.1/Generowanie danych dot. audytu bezpieczeństwa | FMT_MTD.1/Zarządzanie danymi TSF
FAU_SAR.2/Ograniczony przegląd danych audytowych | FAU_SAR.1/Przegląd danych audytowych |
FCS_COP.1/Operacje kryptograficzne | | FCS_CKM.1 <br> FCS_CKM.4 |
FDP_ACC.1/Polityka kontroli dostępu | FDP_ACF.1/atrybuty kontroli dostępu |
FDP_ACF.1/Atrybuty kontroli dostępu | FDP_ACC.1/Polityka kontroli dostępu <br> FMT_MSA.3/Inicjalizacja atrybutów statycznych |
FDP_DAU.1/Podstawowe uwierzytelnianie danych | Bez zależności | 
FIA_AFL.1/Obsługa błędów uwierzytelniania | FIA_UAU.1/Czas uwierzytelniania | 
FIA_UAU.1/Czas uwierzytelniania | FIA_UID.1/Czas autoryzacji |
FIA_UAU.2/Uwierzytelnianie użytkownika przed każdą akcją | FIA_UID.1/Czas autoryzacji |
FIA_UID.1/Czas autoryzacji | Bez zależności |
FIA_UID.2/Autoryzacja użytkownika przed każdą akcją | Bez zależności |
FMT_SMR.1/Role bezpieczeństwa | FIA_UID.1/Czas autoryzacji |
FPT_STM.1/Znaczniki czasu | Bez zależności | 
FTA TAH.1/Historia dostępu do TOE | Bez zależności | 
FMT_MSA.3/Inicjalizacja atrybutów statycznych | FMT_SMR.1/Role bezpieczeństwa | FMT_MSA.1/Zarządzanie atrybutami bezpieczeństwa

### Uzasadnienie niespełnionych zależności

Zależność FMT_MTD.1/Zarządzanie danymi TSF od FAU_SAR.1/Przegląd danych audytowych nie jest spełniona, gdyż aplikacja umożliwia jedynie przegląd danych audytowych bez możliwości ich modyfikowania czy wykonywania na nich innych operacji przez użytkownika.

Zależność FCS_CKM.1, FCS_CKM.4 od FCS_COP.1/Operacje kryptograficzne nie jest spełniona, gdyż funkcja skrótu nie wymaga ani tworzenia, ani importu klucza wewnątrz TOE.

Zależność FMT_MSA.1/Zarządzanie atrybutami bezpieczeństwa od FMT_MSA.3/Inicjalizacja atrybutów statycznych nie jest spełniona, gdyż aplikacja nie pozwala użytkownikom na zarządzanie atrybutami bezpieczeństwa.