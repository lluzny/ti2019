# Projekt niskiego poziomu
Opis klas i metod odpowiedzialnych za bezpieczeństwo aplikacji

## AppProperties
Klasa wspomagająca obsługe konfiguracji aplikacji określonej w pliku application.yml

Metoda | Opis 
--- | --- 
getTokenSecret | zwraca sól tokena
getTokenExpirationMsec | zwraca prekonfigurowany czas ważności tokena

## SecurityConfig
Klasa konfigurująca ustawienia bezpieczeństwa frameworka spring security

Metoda | Opis 
--- | --- 
configure | metoda konfigurująca ustawienia bezpieczeństwa aplikacji (sprawdzenie prawidłowości tokenu w przypadku serwisów o ograniczonym dostępie, odpowiedź na nieautoryzowane zapytania)

## AuthController
Klasa będąca RESTowym kontrolerem udostępniającym metody związane z autentykacją użytkowników

Metoda | Opis 
--- | --- 
authenticateUser | autentykuje użytkownika wysyłając w odpowiedzi wygenerowany token
registerUser | pozwala na rejestracje nowego użytkownika
logoutUser | pozwala na wylogowanie użytkownika

## LogController
Klasa będąca RESTowym kontrolerem pozwalająca na uzyskanie logów

Metoda | Opis 
--- | --- 
getLogs | pozwala na pobranie logów (tylko dla użytkownikó z rolą 'ADMIN')

## ProductController
Klasa bedąca RESTowym kontrolerem pozwalająca na oznacznie produktu jako zakupionego

Metoda | Opis 
--- | --- 
checkProductById | pozwala oznaczyć produkt na liście zakupów jako już kupiony

## ShoppingListController
Klasa będąca RESTowym kontrolerem pozwalającym na obsługe list zakupów

Metoda | Opis 
--- | --- 
getUserShoppingLists | pozwala na pobranie listy list zakupów zalogowanego użytkwonika
addShoppingList | pozwala na dodanie nowej listy zalogowanemu użytkownikowi

## UserController
Klasa będąca RESTowym kontrolerem pozwalającym na uzyskanie informacji o użytkownikach w bazie danych

Metoda | Opis 
--- | --- 
getCurrentUser | zwraca dane zalogowanego użytkownika

## CustomUserDetailService
Klasa wspomagająca operacje związane z użytkownikiem

Metoda | Opis 
--- | --- 
loadUserByUsername | przeszukuje baze danych w poszukiwaniu użytkownika z danym username
loadUserById | przeszukuje baze danych w poszukiwaniu użytkownika z danym id

## RestAuthenticationEntryPoint
Klasa obsługująca nieautoryzowane zapytania do serwisów

Metoda | Opis 
--- | --- 
commence | loguje informacje o nieautoryzowanym zapytaniu i odpowiada kodem HTTP - 401

## TokenAuthenticationFilter
Filter wywoływany przed każdym zapytaniem do serwisów, w których dostęp jest ograniczony dla zalogowanych użytkowników

Metoda | Opis 
--- | --- 
doFilterInternal | sprawdza czy token jakim posługuje się użytkownik jest prawidłowy, a jeśli nie, to przerywa obsługę zapytania 
getJwtFromRequest | pozwala wyciągnąć token z nagłówka żądania

## TokenProvider
Klasa pomagająca w obsłudze JWT (Json Web Token)

Metoda | Opis 
--- | --- 
createToken | tworzy nowy token
getUserIdFromToken | pozwala wyciągnąć infromacje na temat tego dla jakiego użytkownika został wygenerowany dany token
validateToken | sprawdza czy token którym posługuje się użytkownik ma prawidłowy format i czy jest aktualny

## EmailService
Klasa pozwalająca wysłać wiadomość email w przypadku naruszenia bezpieczeństwa aplikacji

Metoda | Opis 
--- | --- 
sendMessage | wysyła wiadomość email na prekonfigurowany adres
