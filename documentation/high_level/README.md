# Projekt wysokiego poziomu

## Ogólne właściwości systemu

### Architektura aplikacji
![architecture_diagram](https://gitlab.com/lluzny/ti2019/raw/develop/documentation/high_level/app_arch.svg)

Architektura aplikacji składa się z trzech głównych komponentów:  
* aplikacji serwerowej - back-end; wykonana w Javie (Spring Framework),
* aplikacji klienckiej - front-end; wykonana w Reactcie; dostępna z poziomu przeglądarki internetowej,
* bazy danych - wykorzystano bazę MySQL.

#### Aplikacja serwerowa

Aplikacja odpowiada za wykonywanie wszelkich operacji zleconych przez aplikację kliencką, obsługę bazy danych oraz generalne funkcjonowanie całego systemu.  
Składa się z trzech modułów:
* moduł uwierzytelniania - obejmuje operacje związane z rejestracją i logowaniem użytkowników,
* moduł list - odpowiada za wszelkie operacje związane z listami zakupów (tworzenie, modyfikacja, udostępnianie itd.),
* moduł security - kontroluje dostęp użytkowników do zasobów aplikacji
* moduł logów - udostępnia dodatkowy panel wraz z logami pracy aplikacji. Moduł dostępny jedynie dla administratora.

#### Aplikacja kliencka
Aplikacja udostępnia interfejs użytkownika oraz przesyła po zabezpieczonym kanale żądania użytkownika do aplikacji serwerowej.

## Stany aplikacji
Główna część aplikacji - aplikacja serwerowa - odczytuje i zapisuje dane do bazy oraz udostępnia je do aplikacji klienckiej dostępnej z poziomu przeglądarki internetowej.

## Sposób obsługi błędów
W przypadku wystąpienia błędu w aplikacji, procedura obsługi błędów jest następująca:
* wystąpienie błędu,
* zapisanie informacji o błędzie w logach,
* przekazanie informacji o błędzie użytkownikowi (wyświetlenie komunikatu z kodem błędu).

## Przechowywane dane
W ramach działania aplikacji dane przechowywane są w:
* bazie danych - wszelkie informacje dotyczące systemu, takie jak:
  * dane użytkownika - e-mail, typ użytkownika, zaszyfrowane hasło,
  * listy zakupów - zawartość list, uprawnienia, atrybuty elementów list.
* local storage przeglądarki - przechowuje token zalogowanego użytkownika

## Dostęp do bazy danych
Aplikacja korzysta z bazy danych w celu przechowywania danych użytkowników i list zakupów. Baza danych zestawiona jest na tej samej maszynie, co aplikacja serwerowa i komunikuje się jedynie z nią wewnątrz lokalnego środowiska.