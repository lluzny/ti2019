package pl.edu.zut.wi.pbai2019.entity;

/**
 * @author IdeFFiX on 21.05.2019
 */
public enum Role {
    OWNER, CONTRIBUTOR
}
