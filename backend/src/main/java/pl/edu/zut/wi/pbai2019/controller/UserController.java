package pl.edu.zut.wi.pbai2019.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.edu.zut.wi.pbai2019.entity.Log;
import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.exception.UserNotFoundException;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.LogService;
import pl.edu.zut.wi.pbai2019.service.UserService;

import java.time.LocalDateTime;
import java.util.List;

import static java.lang.String.format;

/**
 * @author lucas on 08.04.2019
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    private LogService logService;

    @Autowired
    public UserController(UserService userService, LogService logService) {
        this.userService = userService;
        this.logService = logService;
    }

    @GetMapping("/current")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) throws UserNotFoundException {
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Retrieving user with id: %d", userPrincipal.getId())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        User currentUser = userService.getUserById(userPrincipal.getId());
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Retrieved user: %d", userPrincipal.getId())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        return currentUser;
    }
}
