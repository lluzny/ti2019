package pl.edu.zut.wi.pbai2019.service;

import pl.edu.zut.wi.pbai2019.entity.Product;
import pl.edu.zut.wi.pbai2019.exception.ProductNotFoundException;
import pl.edu.zut.wi.pbai2019.exception.UnauthorizedProductAccessException;

import java.util.Optional;

/**
 * @author lucas on 29.05.2019
 */
public interface ProductService {

    Optional<Product> getProductById(long id) throws ProductNotFoundException;

    Product checkProductById(long id) throws ProductNotFoundException, UnauthorizedProductAccessException;
}
