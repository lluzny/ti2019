package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.BlacklistedToken;

/**
 * @author lucas on 13.04.2019
 */
public interface BlacklistedTokenRepository extends CrudRepository<BlacklistedToken, Long> {

    Boolean existsByValue(String value);

}
