package pl.edu.zut.wi.pbai2019.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

/**
 * @author IdeFFiX on 31.05.2019
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends Exception {

    public ProductNotFoundException(long productId) {
        super(format("Product with id: %s not found in database", productId));
    }
}
