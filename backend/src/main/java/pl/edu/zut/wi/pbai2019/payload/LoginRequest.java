package pl.edu.zut.wi.pbai2019.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author lucas on 11.04.2019
 */
@Data
public class LoginRequest {

    @NotBlank
    private String username;

    @NotBlank
    private String password;
}
