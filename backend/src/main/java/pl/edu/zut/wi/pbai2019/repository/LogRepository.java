package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.Log;

import java.util.List;

/**
 * @author lucas on 06.06.2019
 */
public interface LogRepository extends CrudRepository<Log, Long> {

    List<Log> findLogsByUserId(Long userId);

}
