package pl.edu.zut.wi.pbai2019.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author lucas on 13.04.2019
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)

public class TokenAlreadyBlacklistedException extends Exception{

    public TokenAlreadyBlacklistedException(String message) {
        super(message);
    }

}
