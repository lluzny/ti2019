package pl.edu.zut.wi.pbai2019.exception;

import static java.lang.String.format;

/**
 * @author lucas on 08.04.2019
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException(Long userId) {
        super(format("User with id: %s not found in database", userId));
    }
}
