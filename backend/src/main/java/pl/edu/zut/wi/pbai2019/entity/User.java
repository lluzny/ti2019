package pl.edu.zut.wi.pbai2019.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author lucas on 08.04.2019
 */
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private String email;

    private String name;

    private String surname;

    @ElementCollection
    private List<UserRole> roles;

    private LocalDateTime lastSuccessLogin;

    private LocalDateTime lastFailureLogin;

    private LocalDateTime currentLogin;
}
