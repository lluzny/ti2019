package pl.edu.zut.wi.pbai2019.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.zut.wi.pbai2019.entity.Log;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.LogService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

/**
 * @author IdeFFiX on 05.06.2019
 */
@RestController
@RequestMapping("/logs")
public class LogController {

    private LogService logService;

    @Autowired
    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<Log>> getLogs(@CurrentUser UserPrincipal userPrincipal) {

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Attempt to retrieve logs from db by user : : %d", userPrincipal.getId())).timestamp(LocalDateTime.now()).build());
        List<Log> retrievedLogs = logService.getAllLogs();
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Logs retrieved from db by user: : %d", userPrincipal.getId())).timestamp(LocalDateTime.now()).build());

        return new ResponseEntity<>(retrievedLogs, HttpStatus.OK);
    }

}
