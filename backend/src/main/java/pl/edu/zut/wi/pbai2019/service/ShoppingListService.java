package pl.edu.zut.wi.pbai2019.service;

import pl.edu.zut.wi.pbai2019.entity.ShoppingList;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;

import java.util.List;

/**
 * @author lucas on 08.04.2019
 */
public interface ShoppingListService {

    List<ShoppingList> getUserShoppingList(Long userId);

    ShoppingList addShoppingList(ShoppingList shoppingList);
}
