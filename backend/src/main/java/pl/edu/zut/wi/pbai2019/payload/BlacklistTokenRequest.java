package pl.edu.zut.wi.pbai2019.payload;

import lombok.Data;

/**
 * @author lucas on 13.04.2019
 */

@Data
public class BlacklistTokenRequest {

    private String value;

}
