package pl.edu.zut.wi.pbai2019.service;

import pl.edu.zut.wi.pbai2019.util.Mail.Mail;

/**
 * @author lucas on 09.06.2019
 */
public interface EmailService {

    void sendMessage(final Mail mail);
}
