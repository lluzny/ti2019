package pl.edu.zut.wi.pbai2019.payload;

import lombok.Data;

/**
 * @author lucas on 11.04.2019
 */

@Data
public class AuthResponse {
    private String accessToken;
    private String tokenType = "Bearer";

    public AuthResponse(String accessToken) {
        this.accessToken = accessToken;
    }

}
