package pl.edu.zut.wi.pbai2019.exception;

import static java.lang.String.format;

/**
 * @author lucas on 09.06.2019
 */
public class UnauthorizedProductAccessException extends Exception {

    public UnauthorizedProductAccessException(long productId) {
        super(format("Currently logged user does not have access to product with id: %s", productId));

    }
}
