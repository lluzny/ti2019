package pl.edu.zut.wi.pbai2019.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author IdeFFiX on 21.05.2019
 */
@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private boolean checked;

    private LocalDateTime checkedTimestamp;

    @ManyToOne
    private User acceptedBy;
}
