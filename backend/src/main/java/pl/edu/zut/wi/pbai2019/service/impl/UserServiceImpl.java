package pl.edu.zut.wi.pbai2019.service.impl;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.entity.UserRole;
import pl.edu.zut.wi.pbai2019.exception.UserNotFoundException;
import pl.edu.zut.wi.pbai2019.repository.UserRepository;
import pl.edu.zut.wi.pbai2019.service.UserService;

import java.util.Arrays;
import java.util.List;

/**
 * @author lucas on 08.04.2019
 */
@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUserById(Long id) throws UserNotFoundException {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public List<User> getUsersByNameLike(String name) {
        return userRepository.findUsersByUsernameContains(name);
    }

}
