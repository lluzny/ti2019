package pl.edu.zut.wi.pbai2019;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.edu.zut.wi.pbai2019.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class PBAI2019Application {

    public static void main(String[] args) {
        SpringApplication.run(PBAI2019Application.class, args);
    }

}
