package pl.edu.zut.wi.pbai2019.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.edu.zut.wi.pbai2019.entity.*;
import pl.edu.zut.wi.pbai2019.exception.BadRequestException;
import pl.edu.zut.wi.pbai2019.exception.LockedAccountException;
import pl.edu.zut.wi.pbai2019.exception.TokenAlreadyBlacklistedException;
import pl.edu.zut.wi.pbai2019.payload.*;
import pl.edu.zut.wi.pbai2019.repository.BlacklistedTokenRepository;
import pl.edu.zut.wi.pbai2019.repository.BlacklistedUserRepository;
import pl.edu.zut.wi.pbai2019.repository.UserRepository;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.TokenProvider;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.LogService;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static java.lang.String.format;

/**
 * @author lucas on 11.04.2019
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private TokenProvider tokenProvider;

    private BlacklistedTokenRepository blacklistedTokenRepository;

    private LogService logService;

    private BlacklistedUserRepository blacklistedUserRepository;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, PasswordEncoder passwordEncoder, TokenProvider tokenProvider, BlacklistedTokenRepository blacklistedTokenRepository, LogService logService, BlacklistedUserRepository blacklistedUserRepository) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.blacklistedTokenRepository = blacklistedTokenRepository;
        this.logService = logService;
        this.blacklistedUserRepository = blacklistedUserRepository;

    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws LockedAccountException {

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Attempt to log in user with username: %s", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());

        Optional<User> user = userRepository.findByUsername(loginRequest.getUsername());
        User existingUser = null;
        Optional<BlacklistedUser> blacklistedUser = Optional.empty();

        if (user.isPresent()) {
            existingUser = user.get();
            blacklistedUser = blacklistedUserRepository.findById(existingUser.getId());
        }

        if (blacklistedUser.isPresent() && blacklistedUser.get().getBannedTill() != null && LocalDateTime.now().isBefore(blacklistedUser.get().getBannedTill())) {
            logService.addLog(Log.builder().level(LogLevel.ERROR).message(format("Unauthorized attempt to log on blocked account with username: : %s", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());
            throw new LockedAccountException(existingUser.getUsername());
        }

        Authentication authentication = null;
        try {

             authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    loginRequest.getUsername(),
                    loginRequest.getPassword()
            ));
        } finally {

            if (user.isPresent()) {

                if (authentication != null && authentication.isAuthenticated()) {
                    existingUser.setLastSuccessLogin(existingUser.getCurrentLogin());
                    existingUser.setCurrentLogin(LocalDateTime.now());
                    logService.addLog(Log.builder().level(LogLevel.INFO).message(format("User with username: %s successfully logged", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());

                } else {
                    existingUser.setLastFailureLogin(LocalDateTime.now());
                    logService.addLog(Log.builder().level(LogLevel.ERROR).message(format("Unauthorized attempt to log on to user with username: : %s", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());

                    if (blacklistedUser.isPresent()){
                        int unsuccessfulAttempts = blacklistedUser.get().getUnsuccessfulAttempts();
                        if (unsuccessfulAttempts < 4) {
                            blacklistedUser.get().setUnsuccessfulAttempts(unsuccessfulAttempts + 1);
                        } else {
                            blacklistedUser.get().setBannedTill(LocalDateTime.now().plusDays(1));
                            blacklistedUser.get().setUnsuccessfulAttempts(0);
                            blacklistedUserRepository.save(blacklistedUser.get());
                            logService.addLog(Log.builder().level(LogLevel.ERROR).message(format("5th Unauthorized attempt to log on to user with username: : %s. Account is blocked for 24 hours.", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());
                        }
                    } else {
                        BlacklistedUser userToBeBlacklisted = new BlacklistedUser();
                        userToBeBlacklisted.setId(existingUser.getId());
                        userToBeBlacklisted.setUnsuccessfulAttempts(1);
                        blacklistedUserRepository.save(userToBeBlacklisted);
                    }
                }

                userRepository.save(existingUser);
            } else {
                logService.addLog(Log.builder().level(LogLevel.WARN).message(format("Unauthorized attempt to log on not existing user with username: : %s", loginRequest.getUsername())).timestamp(LocalDateTime.now()).build());
            }

        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest) throws BadRequestException {

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Attempt to register user with username: %s", registerRequest.getUsername())).timestamp(LocalDateTime.now()).build());

        if (userRepository.existsByUsername(registerRequest.getUsername())) {
            logService.addLog(Log.builder().level(LogLevel.WARN).message(format("User with username: %s already exists", registerRequest.getUsername())).timestamp(LocalDateTime.now()).build());
            throw new BadRequestException("Username already in use.");
        }

        if(!registerRequest.getPassword().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$")) {
            logService.addLog(Log.builder().level(LogLevel.WARN).message(format("Attempt to register user with username: %s using incorrect password format", registerRequest.getUsername())).timestamp(LocalDateTime.now()).build());
            throw new BadRequestException("Incorrect password. Password must be at least 8 characters long, contain at least one number,one uppercase one lowercase letter.");
        }

        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setName(registerRequest.getName());
        user.setSurname(registerRequest.getSurname());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        user.setEmail(registerRequest.getEmail());
        user.setRoles(Arrays.asList(UserRole.USER));

        User result = userRepository.save(user);

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("User with username: %s successfully created: %s", registerRequest.getUsername(), LocalDateTime.now())).build());

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/user/me")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "User registered successfully"));
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logoutUser(@Valid @RequestBody BlacklistTokenRequest tokenToBeBlacklisted, @CurrentUser UserPrincipal userPrincipal) throws TokenAlreadyBlacklistedException {

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Attempt to logout user with id: %d", userPrincipal.getId())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());

        if(blacklistedTokenRepository.existsByValue(tokenToBeBlacklisted.getValue())) {
            logService.addLog(Log.builder().level(LogLevel.WARN).message("Token already invalid").timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
            throw new TokenAlreadyBlacklistedException("This token is already invalid");
        }

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("User with id: %d successfully logged out", userPrincipal.getId())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());

        BlacklistedToken blacklistedToken  = new BlacklistedToken();
        blacklistedToken.setValue(tokenToBeBlacklisted.getValue());

        blacklistedTokenRepository.save(blacklistedToken);

        return ResponseEntity.ok("You've been successfully logged out");
    }

}
