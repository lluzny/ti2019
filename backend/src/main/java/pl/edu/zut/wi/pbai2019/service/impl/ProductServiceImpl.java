package pl.edu.zut.wi.pbai2019.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.zut.wi.pbai2019.entity.Product;
import pl.edu.zut.wi.pbai2019.entity.ShoppingList;
import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.exception.ProductNotFoundException;
import pl.edu.zut.wi.pbai2019.exception.UnauthorizedProductAccessException;
import pl.edu.zut.wi.pbai2019.repository.ProductRepository;
import pl.edu.zut.wi.pbai2019.repository.ShoppingListRepository;
import pl.edu.zut.wi.pbai2019.service.ProductService;
import pl.edu.zut.wi.pbai2019.util.LoggedUserUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author lucas on 29.05.2019
 */
@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;
    private LoggedUserUtil loggedUserUtil;
    private ShoppingListRepository shoppingListRepository;

    @Override
    public Optional<Product> getProductById(long id) {
        return productRepository.findById(id);
    }

    @Override
    public Product checkProductById(long id) throws ProductNotFoundException, UnauthorizedProductAccessException {

        if (StreamSupport.stream(shoppingListRepository.findAll().spliterator(), false)
                .filter(shoppingList -> shoppingList.getProducts().stream().anyMatch(product -> product.getId().equals(id)))
                .anyMatch(shoppingList -> shoppingList.getSharingList().stream().anyMatch(sharing -> sharing.getUser().getId().equals(loggedUserUtil.getLoggedUser().getId())))) {

            Optional<Product> optionalProduct = productRepository.findById(id);
            if (optionalProduct.isPresent()) {
                Product product = optionalProduct.get();
                product.setChecked(true);
                product.setCheckedTimestamp(LocalDateTime.now());
                product.setAcceptedBy(loggedUserUtil.getLoggedUser());
                return productRepository.save(product);
            } else {
                throw new ProductNotFoundException(id);
            }

        } else {
            throw new UnauthorizedProductAccessException(id);
        }


    }
}
