package pl.edu.zut.wi.pbai2019.util.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.repository.UserRepository;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.util.LoggedUserUtil;

/**
 * @author IdeFFiX on 31.05.2019
 */
@Component
@AllArgsConstructor
public class LoggedUserUtilImpl implements LoggedUserUtil {

    private UserRepository userRepository;

    @Override
    public User getLoggedUser() {
        UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepository.findById(principal.getId()).orElse(null);
    }
}
