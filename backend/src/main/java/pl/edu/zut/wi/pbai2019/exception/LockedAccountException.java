package pl.edu.zut.wi.pbai2019.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

/**
 * @author lucas on 09.06.2019
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class LockedAccountException extends Exception{

    public LockedAccountException(String username) {
        super(format("User with id: %s is banned from login to shopping list app for 24 hours because of 5 unsuccessful login attempts in a row.", username));
    }
}
