package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.ShoppingList;

/**
 * @author lucas on 08.04.2019
 */
public interface ShoppingListRepository extends CrudRepository<ShoppingList, Long> {
}
