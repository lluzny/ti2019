package pl.edu.zut.wi.pbai2019.service;

import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.exception.UserNotFoundException;

import java.util.List;

/**
 * @author lucas on 08.04.2019
 */
public interface UserService {

    User getUserById(Long id) throws UserNotFoundException;

    User addUser(User user);

    List<User> getUsers();

    List<User> getUsersByNameLike(String name);
}
