package pl.edu.zut.wi.pbai2019.entity;

/**
 * @author IdeFFiX on 03.06.2019
 */
public enum UserRole {
    USER, ADMIN
}
