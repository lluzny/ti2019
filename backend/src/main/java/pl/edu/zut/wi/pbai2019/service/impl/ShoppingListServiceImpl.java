package pl.edu.zut.wi.pbai2019.service.impl;

import com.google.common.collect.Streams;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.zut.wi.pbai2019.entity.Role;
import pl.edu.zut.wi.pbai2019.entity.Sharing;
import pl.edu.zut.wi.pbai2019.entity.ShoppingList;
import pl.edu.zut.wi.pbai2019.entity.User;
import pl.edu.zut.wi.pbai2019.repository.ShoppingListRepository;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.ShoppingListService;
import pl.edu.zut.wi.pbai2019.util.LoggedUserUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author lucas on 08.04.2019
 */
@Service
@AllArgsConstructor
public class ShoppingListServiceImpl implements ShoppingListService {

    private ShoppingListRepository shoppingListRepository;
    private LoggedUserUtil loggedUserUtil;


    @Override
    public List<ShoppingList> getUserShoppingList(Long userId) {
        return StreamSupport.stream(shoppingListRepository.findAll().spliterator(), false)
                .filter(shoppingList -> shoppingList.getSharingList().stream()
                    .anyMatch(sharing -> userId.equals(sharing.getUser().getId()))
                )
                .collect(Collectors.toList());
    }

    @Override
    public ShoppingList addShoppingList(ShoppingList shoppingList) {
        shoppingList.getSharingList().add(createLoggedUserSharing());
        shoppingList.setCreated(LocalDateTime.now());
        return shoppingListRepository.save(shoppingList);
    }

    private Sharing createLoggedUserSharing() {
        Sharing sharing = new Sharing();
        sharing.setRole(Role.OWNER);
        sharing.setUser(loggedUserUtil.getLoggedUser());
        return sharing;
    }

}
