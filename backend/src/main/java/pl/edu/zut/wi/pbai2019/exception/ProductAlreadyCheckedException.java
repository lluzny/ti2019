package pl.edu.zut.wi.pbai2019.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static java.lang.String.format;

/**
 * @author lucas on 09.06.2019
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ProductAlreadyCheckedException extends Exception {

    public ProductAlreadyCheckedException(long productId) {
        super(format("Product with id: %s is already checked", productId));
    }
}
