package pl.edu.zut.wi.pbai2019.util;

import pl.edu.zut.wi.pbai2019.entity.User;

/**
 * @author IdeFFiX on 31.05.2019
 */
public interface LoggedUserUtil {
    User getLoggedUser();
}
