package pl.edu.zut.wi.pbai2019.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import pl.edu.zut.wi.pbai2019.entity.Log;
import pl.edu.zut.wi.pbai2019.repository.LogRepository;
import pl.edu.zut.wi.pbai2019.service.EmailService;
import pl.edu.zut.wi.pbai2019.service.LogService;
import pl.edu.zut.wi.pbai2019.util.Mail.Mail;

import java.util.List;

/**
 * @author lucas on 06.06.2019
 */
@Service
public class LogServiceImpl implements LogService {

    private LogRepository logRepository;

    private EmailService emailService;

    @Autowired
    public LogServiceImpl(LogRepository logRepository, EmailService emailService) {
        this.logRepository = logRepository;
        this.emailService = emailService;
    }

    @Override
    public List<Log> getLogByUserId(Long userId) {
        return logRepository.findLogsByUserId(userId);
    }

    @Override
    public Log addLog(Log log) {
        if (log.getLevel().equals(LogLevel.ERROR)) {
            Mail mail = new Mail();
            mail.setFrom("no-reply@listazakupow.com");
            mail.setTo("lista.zakupow.2019@gmail.com");
            mail.setSubject("Lista zakupow security alert");
            mail.setContent(log.getMessage() + " check app logs for more info.");
            this.emailService.sendMessage(mail);
        }
        return logRepository.save(log);
    }

    public List<Log> getAllLogs() {
        return (List<Log>) logRepository.findAll();
    }
}
