package pl.edu.zut.wi.pbai2019.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author lucas on 11.04.2019
 */

@Data
@AllArgsConstructor
public class ApiResponse {

    private boolean success;

    private String message;
}