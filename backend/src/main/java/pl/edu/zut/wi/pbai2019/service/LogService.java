package pl.edu.zut.wi.pbai2019.service;

import pl.edu.zut.wi.pbai2019.entity.Log;

import java.util.List;

/**
 * @author lucas on 06.06.2019
 */
public interface LogService {

    List<Log> getLogByUserId(Long userId);

    Log addLog(Log log);

    List<Log> getAllLogs();
}
