package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.Product;

/**
 * @author lucas on 29.05.2019
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
}
