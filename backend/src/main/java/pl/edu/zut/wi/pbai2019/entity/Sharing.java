package pl.edu.zut.wi.pbai2019.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author IdeFFiX on 21.05.2019
 */
@Entity
@Data
public class Sharing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Role role;

    @ManyToOne
    private User user;
}
