package pl.edu.zut.wi.pbai2019.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.zut.wi.pbai2019.entity.Log;
import pl.edu.zut.wi.pbai2019.entity.ShoppingList;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.LogService;
import pl.edu.zut.wi.pbai2019.service.ShoppingListService;

import java.time.LocalDateTime;
import java.util.List;

import static java.lang.String.format;

/**
 * @author lucas on 08.04.2019
 */

@RestController
@RequestMapping("/shopping-lists")
@Log4j2
public class ShoppingListController {

    private ShoppingListService shoppingListService;

    private LogService logService;

    @Autowired
    public ShoppingListController(ShoppingListService shoppingListService, LogService logService) {
        this.shoppingListService = shoppingListService;
        this.logService = logService;
    }

    @GetMapping
    public ResponseEntity<List<ShoppingList>> getUserShoppingLists(@CurrentUser UserPrincipal userPrincipal) {
        Long userId = userPrincipal.getId();
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Getting all shoppingLists of user with id: %s", userId)).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        List<ShoppingList> shoppingLists = shoppingListService.getUserShoppingList(userId);
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Shopping lists of user with id: %d retrieved", userId)).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        return new ResponseEntity<>(shoppingLists, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ShoppingList> addShoppingList(@CurrentUser UserPrincipal userPrincipal, @RequestBody ShoppingList shoppingList) {
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Adding shoppingList: %s", shoppingList.getName())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        ShoppingList createdShoppingList = shoppingListService.addShoppingList(shoppingList);
        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Added shoppingList: %s", shoppingList.getName())).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        return new ResponseEntity<>(createdShoppingList, HttpStatus.CREATED);
    }

}
