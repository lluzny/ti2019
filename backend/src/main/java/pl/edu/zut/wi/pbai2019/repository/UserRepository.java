package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * @author lucas on 08.04.2019
 */
public interface UserRepository extends CrudRepository<User, Long> {

    Boolean existsByUsername(String username);

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    List<User> findUsersByUsernameContains(String name);

}
