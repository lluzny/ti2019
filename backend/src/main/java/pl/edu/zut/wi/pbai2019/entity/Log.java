package pl.edu.zut.wi.pbai2019.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.logging.LogLevel;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author IdeFFiX on 05.06.2019
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime timestamp;
    private LogLevel level;
    private long userId;
    @Column(length = 3000)
    private String message;

}
