package pl.edu.zut.wi.pbai2019.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * @author lucas on 09.06.2019
 */
@Data
@Entity
public class BlacklistedUser {

    @Id
    private Long id;
    private int unsuccessfulAttempts;
    private LocalDateTime bannedTill;
}
