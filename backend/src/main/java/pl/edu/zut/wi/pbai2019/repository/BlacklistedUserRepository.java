package pl.edu.zut.wi.pbai2019.repository;

import org.springframework.data.repository.CrudRepository;
import pl.edu.zut.wi.pbai2019.entity.BlacklistedUser;

import java.util.Optional;

/**
 * @author lucas on 09.06.2019
 */
public interface BlacklistedUserRepository extends CrudRepository<BlacklistedUser, Long> {

    @Override
    Optional<BlacklistedUser> findById(Long aLong);
}
