package pl.edu.zut.wi.pbai2019.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.zut.wi.pbai2019.entity.Log;
import pl.edu.zut.wi.pbai2019.entity.Product;
import pl.edu.zut.wi.pbai2019.exception.ProductAlreadyCheckedException;
import pl.edu.zut.wi.pbai2019.exception.ProductNotFoundException;
import pl.edu.zut.wi.pbai2019.exception.UnauthorizedProductAccessException;
import pl.edu.zut.wi.pbai2019.repository.ProductRepository;
import pl.edu.zut.wi.pbai2019.security.CurrentUser;
import pl.edu.zut.wi.pbai2019.security.UserPrincipal;
import pl.edu.zut.wi.pbai2019.service.LogService;
import pl.edu.zut.wi.pbai2019.service.ProductService;

import java.time.LocalDateTime;
import java.util.Optional;

import static java.lang.String.format;

/**
 * @author lucas on 29.05.2019
 */
@RestController
@RequestMapping("/products")
@Log4j2
public class ProductController {

    private ProductService productService;

    private LogService logService;

    @Autowired
    public ProductController(ProductService productService, LogService logService) {
        this.productService = productService;
        this.logService = logService;
    }

    @PostMapping("/check/{id}")
    public ResponseEntity<Product> checkProductById(@CurrentUser UserPrincipal userPrincipal, @PathVariable long id) throws ProductNotFoundException, ProductAlreadyCheckedException, UnauthorizedProductAccessException {
        Optional<Product> product = productService.getProductById(id);

        if (product.isPresent()) {
            if (product.get().isChecked()) {
                throw new ProductAlreadyCheckedException(product.get().getId());
            }
        }

        logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Checking product with id: %d", id)).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        Product checkedProduct = null;
        try {
            checkedProduct = productService.checkProductById(id);
            logService.addLog(Log.builder().level(LogLevel.INFO).message(format("Product with id : %d checked", id)).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
        } catch (UnauthorizedProductAccessException e) {
            logService.addLog(Log.builder().level(LogLevel.ERROR).message(format("Currently logged user does not have access to product with id : %d", id)).timestamp(LocalDateTime.now()).userId(userPrincipal.getId()).build());
            throw new UnauthorizedProductAccessException(product.get().getId());
        }
        return new ResponseEntity<>(checkedProduct, HttpStatus.OK);
    }
}
