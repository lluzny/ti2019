package pl.edu.zut.wi.pbai2019;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PBAI2019ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
