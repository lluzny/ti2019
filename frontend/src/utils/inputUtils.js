import TextField from "@material-ui/core/TextField/TextField";
import React from "react";

export const renderTextField = (
    { input, label, meta: { touched, error }, ...custom }
) => (
    <TextField
        label={label}
        margin="normal"
        error={touched && error != null}
        helperText={touched && error}
        {...input}
        {...custom} />
);