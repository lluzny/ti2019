export default values => {
    const errors = {};
    const requiredFields = [
        'username',
        'password',
        'confirmPassword',
        'email',
        'name',
        'surname',
    ];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required';
        }
    });
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address';
    }
    if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
        errors.confirmPassword = 'Passwords must be the same';
    }
    if (values.password && !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(values.password)) {
        errors.password = "Password need to contain minimum eight characters, at least one uppercase letter, one lowercase letter and one number";
    }
    return errors;
}
