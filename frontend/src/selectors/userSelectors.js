import { createSelector } from 'reselect'

const logged = state => state.login.logged;

const user = state => state.user.currentUser;

export const loggedAdminSelector = createSelector(
    logged,
    user,
    (logged, user) => logged && user && user.roles.includes("ADMIN")
);