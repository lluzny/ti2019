import { createSelector } from 'reselect'

const shoppingLists = state => state.shoppingList.shoppingLists;

export const shoppingListsSelector = createSelector(
    shoppingLists,
    shoppingList => shoppingList
);