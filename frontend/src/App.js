import React, {useEffect} from 'react';
import TopBar from "./components/TopBar";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MainPage from "./components/MainPage/MainPage";
import LoginPage from "./components/LoginPage/LoginPage";
import RegisterPage from "./components/RegisterPage/RegisterPage";
import LoadingModal from "./components/LoadingModal/LoadingModal";
import ShoppingListsPage from "./components/ShoppingListsPage/ShoppingListsPage";
import {connect} from "react-redux";
import {fetchAllUsers, fetchCurrentUser} from "./actions/userActions";
import {fetchAllShoppingLists} from "./actions/shoppingListActions";
import AdminPanel from "./components/AdminPanel/AdminPanel";
import ProfilePage from "./components/ProfilePage/ProfilePage";
import ErrorModal from "./components/ErrorModal/ErrorModal";

const App = ( { logged, fetchCurrentUser, fetchAllUsers, fetchAllShoppingLists } ) => {

    useEffect(() => {
        if (logged) {
            fetchCurrentUser();
            fetchAllUsers();
            fetchAllShoppingLists();
            let timeout = setInterval(fetchAllShoppingLists, 5000);
            return () => clearTimeout(timeout);
        }
    }, [logged]);

    return (
        <Router>
            <TopBar/>
            <Route path="/" exact component={MainPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/register" component={RegisterPage}/>
            <Route path="/lists" component={ShoppingListsPage}/>
            <Route path="/admin" component={AdminPanel}/>
            <Route path="/profile" component={ProfilePage}/>
            <LoadingModal/>
            <ErrorModal/>
        </Router>
    );
}

const mapStateToProps = state => ({
    logged: state.login.logged
});

export default connect(mapStateToProps, { fetchCurrentUser, fetchAllUsers, fetchAllShoppingLists })(App);
