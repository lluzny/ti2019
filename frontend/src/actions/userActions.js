import axios from "../config/axios";

export const FETCH_CURRENT_USER = "FETCH_CURRENT_USER";
export const FETCH_USERS_LIKE = "FETCH_USERS_LIKE";
export const FETCH_ALL_USERS = "FETCH_ALL_USERS";

export const fetchCurrentUser = () => ({
    type: FETCH_CURRENT_USER,
    payload: axios(true).get("https://localhost:8443/users/current")
});

export const fetchUsersLike = (name) => ({
    type: FETCH_USERS_LIKE,
    payload: axios(true).get(`https://localhost:8443/users/${name}`)
});

export const fetchAllUsers = () => ({
    type: FETCH_ALL_USERS,
    payload: axios(true).get(`https://localhost:8443/users`)
});

