import axios from "../config/axios";

export const LOGIN_ACTION = "LOGIN_ACTION";

export const LOGOUT_ACTION = "LOGOUT_ACTION";

export const login = loginDate => ({
    type: LOGIN_ACTION,
    payload: axios(true).post("https://localhost:8443/auth/login", loginDate)
});

export const logout = () => ({
    type: LOGOUT_ACTION,
    payload: axios(true).post("https://localhost:8443/auth/logout", {
        value: localStorage.getItem("token")
    })
})