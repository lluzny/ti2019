import axios from "../config/axios";

export default (user) => ({
    type: "REGISTER",
    payload: axios(true).post("https://localhost:8443/auth/register", user)
})