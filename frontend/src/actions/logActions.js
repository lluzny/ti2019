import axios from "../config/axios";

export const FETCH_LOGS = "FETCH_LOGS";

export const fetchLogs = () => ({
    type: FETCH_LOGS,
    payload: axios(true).get("https://localhost:8443/logs")
});

