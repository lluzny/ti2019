import {CLOSE_ERROR_MODAL} from "../config/axios";

export const closeModal = () => ({
    type: CLOSE_ERROR_MODAL
})