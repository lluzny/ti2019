import axios from "../config/axios";

export const CREATE_SHOPPING_LIST = "CREATE_SHOPPING_LIST";

export const FETCH_ALL_SHOPPING_LISTS = "FETCH_ALL_SHOPPING_LISTS";

export const CHECK_PRODUCT = "CHECK_PRODUCT";

export const createShoppingList = shoppingList => ({
    type: CREATE_SHOPPING_LIST,
    payload: axios(true).post("https://localhost:8443/shopping-lists", shoppingList)
});

export const fetchAllShoppingLists = () => ({
    type: FETCH_ALL_SHOPPING_LISTS,
    payload: axios(false).get("https://localhost:8443/shopping-lists")
});

export const checkProduct = id => ({
    type: CHECK_PRODUCT,
    payload: axios(true).post(`https://localhost:8443/products/check/${id}`)
})