import {connect} from "react-redux";
import {compose} from "redux";
import React from "react";
import NotAuthorizedPage from "../components/NotAuthorizedPage/NotAuthorizedPage";

const withAuth = WrappedComponent => ({ logged }) => (
    <>
        {logged ? <WrappedComponent/> : <NotAuthorizedPage/>}
    </>
);

const mapStateToProps = (state) => ({
    logged: state.login.logged
});

export default compose(
    connect(mapStateToProps, null),
    withAuth
)