import NotAuthorizedPage from "../components/NotAuthorizedPage/NotAuthorizedPage";
import {compose} from "redux";
import connect from "react-redux/es/connect/connect";
import React from "react";
import {loggedAdminSelector} from "../selectors/userSelectors";

const withAdmin = WrappedComponent => ({ isAdminLogged }) => (
    <>
        {isAdminLogged ? <WrappedComponent/> : <NotAuthorizedPage/>}
    </>
);

const mapStateToProps = state => ({
    isAdminLogged: loggedAdminSelector(state)
});

export default compose(
    connect(mapStateToProps, null),
    withAdmin
)