import {OPEN_ERROR_MODAL, CLOSE_ERROR_MODAL} from "../config/axios";

export default (state = {open: false, text: null}, action) => {
    switch (action.type) {
        case OPEN_ERROR_MODAL:
            return {
                open: true,
                text: action.payload
            };
        case CLOSE_ERROR_MODAL:
            return {
                open: false
            };
        default:
            return state
    }
}