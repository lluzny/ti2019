import {LOGIN_ACTION, LOGOUT_ACTION} from "../actions/loginActions";

export default (state = {logged: localStorage.getItem("token") != null}, action) => {
    switch (action.type) {
        case `${LOGIN_ACTION}_FULFILLED`:
            let token = action.payload.data.accessToken;
            localStorage.setItem("token", token);
            return {
                logged: true
            };
        case `${LOGOUT_ACTION}_FULFILLED`:
            localStorage.removeItem("token");
            return {
                logged: false
            };
        default:
            return state;
    }
}