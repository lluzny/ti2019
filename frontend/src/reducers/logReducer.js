import {FETCH_LOGS} from "../actions/logActions";

export default (state = {logs: []}, action) => {
    switch (action.type) {
        case `${FETCH_LOGS}_FULFILLED`:
            return {
                logs: action.payload.data,
            };
        default:
            return state;
    }
}