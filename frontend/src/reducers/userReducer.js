import {FETCH_ALL_USERS, FETCH_CURRENT_USER, FETCH_USERS_LIKE} from "../actions/userActions";
import {LOGOUT_ACTION} from "../actions/loginActions";

export default (state = {currentUser: null, usersLike: [], allUsers: []}, action) => {
    switch (action.type) {
        case `${FETCH_CURRENT_USER}_FULFILLED`:
            return {
                ...state,
                currentUser: action.payload.data,
            };
        case `${FETCH_USERS_LIKE}_FULFILLED`:
            return {
                ...state,
                usersLike: action.payload.data,
            };
        case `${FETCH_ALL_USERS}_FULFILLED`:
            return {
                ...state,
                allUsers: action.payload.data
            };
        case `${LOGOUT_ACTION}_FULFILLED`:
            return {
                ...state,
                currentUser: null
            }
        default:
            return state;
    }
}