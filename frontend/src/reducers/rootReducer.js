import {combineReducers} from "redux";
import loadingModal from "./loadingModalReducer";
import login from "./loginReducer";
import user from "./userReducer";
import log from "./logReducer";
import shoppingList from "./shoppingListReducer";
import errorModal from "./errorModalReducer";
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    form: formReducer,
    loadingModal,
    login,
    user,
    shoppingList,
    log,
    errorModal
})