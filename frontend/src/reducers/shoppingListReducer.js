import {FETCH_ALL_SHOPPING_LISTS} from "../actions/shoppingListActions";

export default (state = {shoppingLists: []}, action) => {
    switch (action.type) {
        case `${FETCH_ALL_SHOPPING_LISTS}_FULFILLED`:
            return {
                ...state,
                shoppingLists: action.payload.data,
            };
        default:
            return state;
    }
}