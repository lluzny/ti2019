import {CLOSE_LOADING_MODAL, OPEN_LOADING_MODAL} from "../config/axios";

export default (state = {open: false}, action) => {
    switch (action.type) {
        case OPEN_LOADING_MODAL:
            return {
                open: true
            }
        case CLOSE_LOADING_MODAL:
            return {
                open: false
            }
        default:
            return state
    }
}