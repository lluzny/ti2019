import React, {useEffect} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import withAdmin from "../../hoc/withAdmin";
import Typography from "@material-ui/core/Typography/Typography";
import TableSortLabel from "@material-ui/core/TableSortLabel/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination/TablePagination";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from '@material-ui/core/Input';
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import Search from '@material-ui/icons/Search';
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import {connect} from "react-redux";
import {fetchLogs} from "../../actions/logActions";

const AdminPanel = ({ fetchLogs, logs}) => {

    const createHeader = (label, fieldName) => ({
        label, fieldName
    });

    const headers = [
        createHeader('Timestamp', 'timestamp'),
        createHeader('Log level', 'level'),
        createHeader('User', 'userId'),
        createHeader('Message', 'message')
    ];

    useEffect(fetchLogs, []);

    const [sortBy, setSortBy] = React.useState('timestamp');
    const [filterBy, setFilterBy] = React.useState('timestamp');
    const [filterValue, setFilterValue] = React.useState('');
    const [direction, setDirection] = React.useState('desc');
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const [page, setPage] = React.useState(0);

    const changeSortHandler = row => () => {
        if (row === sortBy) {
            setDirection(direction === 'desc' ? 'asc' : 'desc');
        }
        setSortBy(row);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
    };

    const handleFilterByChange = event => {
        setFilterBy(event.target.value);
    };

    const handleFilterValueChange = event => {
        setFilterValue(event.target.value);
    };

    return (
        <Paper>
            <Typography variant="h6">
                Logs
                <FormControl className={"margin-left-52"}>
                    <InputLabel>Filter value</InputLabel>
                    <Input
                        onChange={handleFilterValueChange}
                        startAdornment={
                            <InputAdornment position="start">
                                <Search />
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <FormControl>
                    <InputLabel>Filter by</InputLabel>
                    <Select
                        value={filterBy}
                        onChange={handleFilterByChange}>
                        {headers.map(h => (
                            <MenuItem
                                key={h.fieldName}
                                value={h.fieldName}>{h.label}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        {headers
                            .map(h => (
                            <TableCell
                                key={h.label}
                                sortDirection={h.fieldName === sortBy ? direction : false}>
                                <TableSortLabel
                                    active={h.fieldName === sortBy}
                                    direction={direction}
                                    onClick={changeSortHandler(h.fieldName)}>
                                    {h.label}
                                </TableSortLabel>
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {logs
                        .filter(log => {
                            if (filterValue === '') {
                                return true;
                            } else {
                                return log[filterBy].toString().includes(filterValue);
                            }
                        })
                        .sort((a, b) => ((a[sortBy] > b[sortBy]) - (a[sortBy] < b[sortBy])) * (direction === 'asc' ? 1 : -1))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((log, i) => (
                        <TableRow key={i}>
                            <TableCell component="th" scope="row">
                                {log.timestamp}
                            </TableCell>
                            <TableCell>{log.level}</TableCell>
                            <TableCell>{log.userId}</TableCell>
                            <TableCell>{log.message}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={logs.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page',
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </Paper>
    );
}

const mapStateToProps = state => ({
    logs: state.log.logs,
});

const Connected = connect(mapStateToProps, { fetchLogs })(AdminPanel);

export default withAdmin(Connected);