import React from "react";

const NotAuthorizedPage = () => (
    <div>You are not authorized! Please log in first</div>
);

export default NotAuthorizedPage;