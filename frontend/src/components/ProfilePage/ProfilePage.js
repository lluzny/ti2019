import React from "react";
import withAuth from "../../hoc/withAuth";
import Paper from "@material-ui/core/Paper/Paper";
import {connect} from "react-redux";

const ProfilePage = ({ user }) => (
    <Paper>
        {user &&
            <>
                <h4>Username: {user.username}</h4>
                <h4>Name: {user.name}</h4>
                <h4>Surname: {user.surname}</h4>
                <h4>Email: {user.email}</h4>
                <h4>Last success login: {user.lastSuccessLogin}</h4>
                <h4>Last failure login: {user.lastFailureLogin}</h4>
            </>
        }
    </Paper>
);

const mapStateToProps = state => ({
    user: state.user.currentUser
});

const Connected = connect(mapStateToProps, {})(ProfilePage);

export default withAuth(Connected);