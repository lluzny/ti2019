import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {withRouter} from "react-router";
import {loggedAdminSelector} from "../../selectors/userSelectors";
import {connect} from "react-redux";
import {logout} from "../../actions/loginActions";

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class TopBar extends React.Component {
    state = {
        auth: true,
        profileAnchor: null,
        hamburgerAnchor: null,
    };

    handleHamburger = event => {
        this.setState({ hamburgerAnchor: event.currentTarget })
    }

    handleProfile = event => {
        this.setState({ profileAnchor: event.currentTarget });
    };

    handleClose = () => {
        this.setState({
            profileAnchor: null,
            hamburgerAnchor: null,
        });
    };

    handleLogoutClick = () => {
        this.props.logout();
        this.handleClose();
    }


    goTo = (link) => {
        this.props.history.push(link);
        this.handleClose();
    };

    render() {
        const { classes, isAdminLogged, logged } = this.props;
        const { profileAnchor, hamburgerAnchor } = this.state;
        const open = Boolean(profileAnchor);
        const openHamburger = Boolean(hamburgerAnchor);

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <div>
                            <IconButton className={classes.menuButton}
                                        aria-label="Menu"
                                        aria-owns={openHamburger ? 'left-menu-appbar' : undefined}
                                        aria-haspopup="true"
                                        onClick={this.handleHamburger}
                                        color="inherit">
                                <MenuIcon />
                            </IconButton>
                            <Menu
                                id="left-menu-appbar"
                                anchorEl={hamburgerAnchor}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={openHamburger}
                                onClose={this.handleClose}
                            >
                            {!logged &&
                                <>
                                    <MenuItem onClick={() => this.goTo("/login")}>Login</MenuItem>
                                    <MenuItem onClick={() => this.goTo("/register")}>Register</MenuItem>
                                </>
                            }
                            {logged &&
                                <MenuItem onClick={() => this.goTo("/lists")}>Shopping lists</MenuItem>
                            }
                            {isAdminLogged && <MenuItem onClick={() => this.goTo("/admin")}>Admin panel</MenuItem>}
                        </Menu>
                        </div>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            buy.it - easy memorize your shopping list!
                        </Typography>
                        {logged && (
                            <div>
                                <IconButton
                                    aria-owns={open ? 'menu-appbar' : undefined}
                                    aria-haspopup="true"
                                    onClick={this.handleProfile}
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={profileAnchor}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem onClick={() => this.goTo("/profile")}>Profile</MenuItem>
                                    <MenuItem onClick={this.handleLogoutClick}>Logout</MenuItem>
                                </Menu>
                            </div>
                        )}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

TopBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    isAdminLogged: loggedAdminSelector(state),
    logged: state.login.logged
});

export default connect(mapStateToProps, { logout })(withRouter(withStyles(styles)(TopBar)));
