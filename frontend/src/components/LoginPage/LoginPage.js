import React from "react";
import Grid from "@material-ui/core/Grid/Grid";
import Typography from "@material-ui/core/Typography/Typography";
import Paper from "@material-ui/core/Paper/Paper";
import {Field, reduxForm} from "redux-form";
import Button from "@material-ui/core/Button/Button";
import validate from "../../validations/loginFormValidator";
import {withStyles} from "@material-ui/core";
import {connect} from "react-redux";
import {login} from "../../actions/loginActions";
import {fetchCurrentUser} from "../../actions/userActions";
import {withRouter} from "react-router";
import {renderTextField} from "../../utils/inputUtils";

const styles = (theme) => ({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            width: '90%'
        },
        button: {
            margin: theme.spacing.unit,
        },
    }
);

const LoginPage = ({ classes, pristine, invalid, values, login, fetchCurrentUser, history }) => {

    const handleLoginClick = () => {
        login(values).then(fetchUser);
    }

    const fetchUser = () => {
        fetchCurrentUser().then(redirectToShoppingLists);
    }

    const redirectToShoppingLists = () => history.push('/lists');

    return (
        <Grid
            container
            justify="center">
            <Grid item xs={3}>
                <Paper>
                    <Typography variant="h5" component="h3" align={"center"}>
                        Login
                    </Typography>
                    <form
                        className={classes.container}
                        noValidate
                        autoComplete="off">
                        <Grid item xs={12}>
                            <Field
                                name="username"
                                label="Username"
                                component={renderTextField}
                                className={classes.textField}/>
                        </Grid>
                        <Grid item xs={12}>
                            <Field
                                name="password"
                                label="Password"
                                component={renderTextField}
                                className={classes.textField}
                                type={"password"}/>
                        </Grid>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            fullWidth={true}
                            disabled={pristine || invalid}
                            onClick={handleLoginClick}>
                            Login
                        </Button>
                    </form>
                </Paper>
            </Grid>
        </Grid>
    )
}

// TODO it should be possible to get form state without checking null
const mapStateToProps = (state) => ({
    values: state.form.login != null ? state.form.login.values : null,
})

const Connected = connect(mapStateToProps, { login, fetchCurrentUser })(LoginPage);

export default withRouter(
    withStyles(styles)(
        reduxForm({
            form: 'login',
            validate,
        })(Connected)
    )
);