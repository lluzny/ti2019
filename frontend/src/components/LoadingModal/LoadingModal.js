import React from "react";
import {connect} from "react-redux";
import Dialog from "@material-ui/core/Dialog/Dialog";
import {PacmanLoader} from "react-spinners";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import { css } from '@emotion/core';

const override = css`
    display: block;
    margin-left: 25%;
    margin-top: 50px;
`;

const LoadingModal = ({ classes, open }) => (
    <Dialog open={open}>
        <DialogContent style={{width: '400px', height: '200px', textAlign: 'center'}}>
            <PacmanLoader
                size={50}
                css={override}
                sizeUnit={"px"}
                loading={true}
                color={"#3f51b5"} />
        </DialogContent>
    </Dialog>
)

const mapStateToProps = state => ({
    open: state.loadingModal.open
})

export default connect(mapStateToProps, null)(LoadingModal);