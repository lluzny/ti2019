import React from "react";
import Paper from "@material-ui/core/Paper/Paper";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import {checkProduct, fetchAllShoppingLists} from "../../actions/shoppingListActions";
import {connect} from "react-redux";

const ShoppingList = ({ shoppingList, loggedUserId, checkProduct, fetchAllShoppingLists }) => (
    <Paper>
        <h3 className={"center no-margin-top"}>{shoppingList.name}</h3>
        <div>
            <b>Shared with: </b>
            {shoppingList.sharingList
                .filter(sl => sl.user.id !== loggedUserId)
                .map(sl => sl.user.name + " ")}
        </div>
        {shoppingList.products.map(p => (
            <div key={p.id}>
                <FormControlLabel
                    disabled={p.checked}
                    control={
                        <Checkbox
                            checked={p.checked}
                        />
                    }
                    label={p.checked ? `${p.title} (Checked by: ${p.acceptedBy.name} at: ${p.checkedTimestamp})`: p.title}
                    onChange={() => checkProduct(p.id).then(() => fetchAllShoppingLists())}
                />
            </div>
        ))}
    </Paper>
);

export default connect(null, { checkProduct, fetchAllShoppingLists })(ShoppingList);