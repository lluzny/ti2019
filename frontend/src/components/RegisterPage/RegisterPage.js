import React from "react";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import {withStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Field, reduxForm} from "redux-form";
import validate from "../../validations/registerFormValidator";
import register from "../../actions/registrationActions"
import {connect} from "react-redux";
import {renderTextField} from "../../utils/inputUtils";

const styles = (theme) => ({
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            width: '90%'
        },
        button: {
            margin: theme.spacing.unit,
        },
    }
);

const RegisterPage = ({classes, pristine, invalid, register, values}) => (
    <Grid
        container
        justify="center">
        <Grid item xs={6}>
            <Paper>
                <Typography variant="h5" component="h3" align={"center"}>
                    Registration
                </Typography>
                <form
                    className={classes.container}
                    noValidate
                    autoComplete="off">
                    <Grid item xs={4}>
                        <Field
                            name="username"
                            label="Username"
                            component={renderTextField}
                            className={classes.textField} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field
                            name="password"
                            label="Password"
                            component={renderTextField}
                            className={classes.textField}
                            type={"password"} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field
                            name="confirmPassword"
                            label="Confirm password"
                            component={renderTextField}
                            className={classes.textField}
                            type={"password"} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field
                            name="name"
                            label="Name"
                            component={renderTextField}
                            className={classes.textField} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field
                            name="surname"
                            label="Surname"
                            component={renderTextField}
                            className={classes.textField} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field
                            name="email"
                            label="Email"
                            component={renderTextField}
                            className={classes.textField} />
                    </Grid>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        fullWidth={true}
                        disabled={pristine || invalid}
                        onClick={() => register(values)} >
                        Register
                    </Button>
                </form>
            </Paper>
        </Grid>
    </Grid>
);

// TODO it should be possible to get form state without checking null
const mapStateToProps = (state) => ({
    values: state.form.register != null ? state.form.register.values : null,
})

const Connected = withStyles(styles)(
    connect(mapStateToProps, {register})(RegisterPage)
)

export default reduxForm({
    form: 'register',
    validate,
})(Connected);


