import React from "react";
import {connect} from "react-redux";
import withAuth from "../../hoc/withAuth";
import Typography from "@material-ui/core/Typography/Typography";
import Grid from "@material-ui/core/Grid/Grid";
import Divider from "@material-ui/core/Divider/Divider";
import {withStyles} from "@material-ui/core";
import AddList from "../AddList/AddList";
import {shoppingListsSelector} from "../../selectors/shoppingListSelector";
import ShoppingList from "../ShoppingList/ShoppingList";

const styles = () => ({
    spaced: {
        marginBottom: "10px",
        marginTop: "10px"
    }
});

const ShoppingListsPage = ({ classes, user, shoppingLists }) => (
    <div>
        {user &&
            <>
                <Typography variant={"h5"} align={"center"}>
                    {`Hi ${user.name} ${user.surname}. Here you have your Shopping Lists!`}
                </Typography>
                <Divider
                    variant="middle"
                    className={classes.spaced}/>
                <Grid
                    container
                    justify="center">
                    {shoppingLists.map(sl => (
                        <Grid item xs={3} key={sl.id}>
                            <ShoppingList
                                shoppingList={sl}
                                loggedUserId={user.id}/>
                        </Grid>
                    ))}
                    <Grid item xs={3}>
                        <AddList />
                    </Grid>
                </Grid>
            </>
        }
    </div>
)

const mapStateToProps = (state) => ({
    user: state.user.currentUser,
    shoppingLists: shoppingListsSelector(state),
});

export default withAuth(connect(mapStateToProps, {})(withStyles(styles)(ShoppingListsPage)));