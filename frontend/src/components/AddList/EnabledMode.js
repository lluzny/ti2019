import React from "react";
import {connect} from "react-redux";
import Select from "react-select";
import {Field, FieldArray, reduxForm} from "redux-form";
import {renderTextField} from "../../utils/inputUtils";
import validate from "../../validations/registerFormValidator";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";
import InputAdornment from "@material-ui/core/InputAdornment/InputAdornment";
import IconButton from "@material-ui/core/IconButton/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import Fab from "@material-ui/core/Fab/Fab";
import AddIcon from '@material-ui/icons/Add';
import Button from "@material-ui/core/Button/Button";
import {createShoppingList, fetchAllShoppingLists} from "../../actions/shoppingListActions";
import _ from "lodash";

const renderProducts = ({ fields, meta: { error, submitFailed } }) => (
    <>
        <div className={"center"}>
            <Fab
                color="primary"
                size="small"
                aria-label="Add"
                onClick={() => fields.push({})}>
                <AddIcon />
            </Fab>
        </div>
        {submitFailed && error && <span>{error}</span>}
        {fields.map((product, index) => (
            <div key={index}>
                <Field
                    name={`${product}.title`}
                    type="text"
                    label={`Product #${index + 1}`}
                    component={({ input, label, meta: { touched, error }, ...custom }) => (
                        <FormControl fullWidth>
                            <InputLabel>{label}</InputLabel>
                            <Input
                                type={'text'}
                                onChange={(c) => console.log(c)}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton onClick={() => fields.remove(index)}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </InputAdornment>
                                }
                                error={touched && error != null}
                                {...input}
                                {...custom}
                            />
                        </FormControl>
                    )}
                />
            </div>
        ))}
    </>
);

const EnabledMode = ({ allUsers, currentUser, createShoppingList, shoppingList, setAddingMode, fetchAllShoppingLists }) => {

    const suggestions = allUsers.filter(u => u.id !== currentUser.id)
        .map(u => ({
            label: u.username,
            value: u.id,
            user: {
                id: u.id,
            },
            role: "CONTRIBUTOR",
        }));

    return (
        <form>
            <Field
                name={"name"}
                label="List name"
                margin="normal"
                fullWidth
                component={renderTextField} />
            <Field
                name={"sharingList"}
                component={({ input, label, meta: { touched, error }, ...custom }) => (
                    <Select
                        options={suggestions}
                        placeholder="Share with:"
                        isMulti
                        error={touched && error != null}
                        helperText={touched && error}
                        value={input.value}
                        onChange={(value) => input.onChange(value)}
                        onBlur={() => input.onBlur(input.value)}
                        {...custom} />
                )}/>
            <FieldArray
                name="products"
                component={renderProducts} />
            <div className={"margin-top-5"}>
                <Button
                    type={"button"}
                    variant="contained"
                    color="primary"
                    fullWidth
                    onClick={() => createShoppingList(shoppingList).then(() => {
                        setAddingMode(false);
                        fetchAllShoppingLists();
                    })}>
                    Create!
                </Button>
            </div>
        </form>
    )
}

const mapStateToProps = state => ({
    allUsers: state.user.allUsers,
    currentUser: state.user.currentUser,
    shoppingList: _.get(state, 'form.addList.values'),
})

const Connected = connect(mapStateToProps, { createShoppingList, fetchAllShoppingLists })(EnabledMode);

export default reduxForm({
    form: 'addList',
    validate
})(Connected);