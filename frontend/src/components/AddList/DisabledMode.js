import React from "react";
import classNames from "classnames";
import Icon from "@material-ui/core/Icon/Icon";
import {withStyles} from "@material-ui/core";

const styles = (theme) => ({
    center: {
        textAlign: "center"
    },
    pointer: {
        '&:hover': {
            cursor: "pointer"
        }
    },
    icon: {
        margin: theme.spacing.unit * 2,
        color: "green"
    }
});

const DisabledMode = ({ onClick, classes }) => (
    <div
        className={classNames(classes.pointer, classes.center)}
        onClick={onClick}>
        <div>
            <Icon
                className={classNames(classes.icon, 'fa fa-plus-circle')}
                color="disabled"
                fontSize="large"/>
        </div>
        <div>
            Add new list!
        </div>
    </div>
)

export default withStyles(styles)(DisabledMode);