import React, {useState} from "react";
import EnabledMode from "./EnabledMode";
import DisabledMode from "./DisabledMode";
import Paper from "@material-ui/core/Paper/Paper";

const AddList = () => {
    const [addingMode, setAddingMode] = useState(false);

    return (
        <Paper>
            {addingMode ?
                <EnabledMode
                    setAddingMode={setAddingMode}/>
                :
                <DisabledMode
                    onClick={() => setAddingMode(true)} />}
        </Paper>
    )
};
export default AddList;