import React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import {connect} from "react-redux";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import {closeModal} from "../../actions/errorModalActions";

const ErrorModal = ({ open, text, closeModal }) => (
    <Dialog open={open}>
        <DialogContent style={{width: '400px', height: '200px', textAlign: 'center'}} >
            <IconButton aria-label="Close" onClick={closeModal}>
                <CloseIcon />
            </IconButton>
            <h3>{text}</h3>
        </DialogContent>
    </Dialog>
);

const mapStateToProps = state => ({
    open: state.errorModal.open,
    text: state.errorModal.text
});

export default connect(mapStateToProps, { closeModal })(ErrorModal);