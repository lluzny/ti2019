import axios from "axios";
import dispatch from "../index";

export const OPEN_LOADING_MODAL = "OPEN_LOADING_MODAL";
export const CLOSE_LOADING_MODAL = "CLOSE_LOADING_MODAL";
export const OPEN_ERROR_MODAL = "OPEN_ERROR_MODAL";
export const CLOSE_ERROR_MODAL = "CLOSE_ERROR_MODAL";

const axiosConfig = (withModal) => {
    let instance = axios.create();
    instance.interceptors.request.use(
        config => {
            withModal && dispatch({ type: OPEN_LOADING_MODAL });
            let token = localStorage.getItem("token");
            if (token) {
              config.headers = { Authorization: `Bearer ${token}`};
            }
            return config;
        },
        error => {
            withModal && dispatch({ type: OPEN_LOADING_MODAL });
            dispatch({ type: OPEN_ERROR_MODAL, payload: error.response == null ? error.message : error.response.data.message });
            return Promise.reject(error);
        }
    )
    instance.interceptors.response.use(
        config => {
            withModal && dispatch({ type: CLOSE_LOADING_MODAL });
            return config;
        },
        error => {
            withModal && dispatch({ type: CLOSE_LOADING_MODAL });
            dispatch({ type: OPEN_ERROR_MODAL, payload: error.response == null ? error.message : error.response.data.message });
            return Promise.reject(error);
        }
    )
    return instance;
}


export default axiosConfig;

